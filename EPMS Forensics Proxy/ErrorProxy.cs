﻿using EPMS_Forensics_Contract;
using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;

namespace EPMS_Forensics_Proxy
{
    public class ErrorProxy
    {
        public IError ErrorChannel { get; }

        private string ServiceBaseAddress { get; }

        public ErrorProxy()
        {
            var serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];
            var serviceName = "ErrorService.svc";

            if (serviceUrl.EndsWith("/"))
            {
                ServiceBaseAddress = serviceUrl + serviceName;
            }
            else
            {
                ServiceBaseAddress = serviceUrl + "/" + serviceName;
            }

            var binding = new WSHttpBinding
            {
                MaxReceivedMessageSize = 4194304,
                Security =
                {
                    Mode = SecurityMode.None,
                    Transport = {ClientCredentialType = HttpClientCredentialType.None}
                },
                SendTimeout = new TimeSpan(0, 10, 0),
                ReceiveTimeout = new TimeSpan(0, 10, 0),
                ReaderQuotas = {MaxArrayLength = 4194304}
            };
            //4 mb


            //Always accept as this is on the local network and would need to run on a self signed certificate
            //if (Common.Utilities.GeneralUtilities.IsDeveloper)
            //{
            ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return true;
            };
            //}

            var cf = new ChannelFactory<IError>(binding, ServiceBaseAddress);

            ErrorChannel = cf.CreateChannel();
        }
    }
}
