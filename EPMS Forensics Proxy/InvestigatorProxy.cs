﻿using EPMS_Forensics_Contract;
using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;

namespace EPMS_Forensics_Proxy
{
    public class InvestigatorProxy
    {
        public IInvestigator InvestigatorChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public InvestigatorProxy()
        {
            var serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];
            const string serviceName = "InvestigatorService.svc";

            if (serviceUrl.EndsWith("/"))
            {
                ServiceBaseAddress = serviceUrl + serviceName;
            }
            else
            {
                ServiceBaseAddress = serviceUrl + "/" + serviceName;
            }

            var binding = new WSHttpBinding
            {
                MaxReceivedMessageSize = 4194304,
                Security =
                {
                    Mode = SecurityMode.None,
                    Transport = {ClientCredentialType = HttpClientCredentialType.None}
                },
                SendTimeout = new TimeSpan(0, 10, 0),
                ReceiveTimeout = new TimeSpan(0, 10, 0),
                ReaderQuotas = {MaxArrayLength = 4194304}
            };
            //4 mb


            //Always accept as this is on the local network and would need to run on a self signed certificate
            //if (Common.Utilities.GeneralUtilities.IsDeveloper)
            //{
            ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return true;
            };
            //}

            var cf = new ChannelFactory<IInvestigator>(binding, ServiceBaseAddress);

            InvestigatorChannel = cf.CreateChannel();
        }
    }
}
