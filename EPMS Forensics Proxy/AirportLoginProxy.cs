﻿using EPMS_Forensics_Contract;
using System;
using System.Configuration;
using System.Net;
using System.ServiceModel;

namespace EPMS_Forensics_Proxy
{
    public class AirportLoginProxy
    {
        private readonly ChannelFactory<IAirportLogin> _cf;

        public IAirportLogin LoginChannel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public AirportLoginProxy()
        {
            var serviceUrl = ConfigurationManager.AppSettings["ServiceUrl"];
            var serviceName = "AirportLoginService.svc";

            if (serviceUrl.EndsWith("/"))
            {
                ServiceBaseAddress = serviceUrl + serviceName;
            }
            else
            {
                ServiceBaseAddress = serviceUrl + "/" + serviceName;
            }

            var binding = new WSHttpBinding();
            binding.MaxReceivedMessageSize = 4194304;//4 mb
            binding.Security.Mode = SecurityMode.None;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            binding.SendTimeout = new TimeSpan(0, 10, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 10, 0);

            binding.ReaderQuotas.MaxArrayLength = 4194304;

            //Always accept as this is on the local network and would need to run on a self signed certificate
            //if (Common.Utilities.GeneralUtilities.IsDeveloper)
            //{
            ServicePointManager.ServerCertificateValidationCallback = delegate
            {
                return true;
            };
            //}

            _cf = new ChannelFactory<IAirportLogin>(binding, ServiceBaseAddress);

            LoginChannel = _cf.CreateChannel();
        }
    }
}
