﻿using Afisteam.Biometrics;
using Afisteam.Biometrics.Gui;
using Afisteam.Biometrics.Scannning;
using EPMS_Forensics_Proxy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;

namespace CollectBiometrics
{
    public class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            try
            {
                if (args.Length != 0)
                {
                    if (args[0] == "1")
                        TakeFingerprint();

                    Environment.Exit(1);
                }
                else
                {
                    TakeFingerprint();
                    //Environment.Exit(3);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(0);
            }
        }

        #region Biometric Operations

        private static void TakeFingerprint()
        {
            var location = @"C:/InvestigatorErrorLog_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            var sb = new StringBuilder();
            sb.Append(DateTime.Now.ToString("hh:mmtt"));
            sb.Append("==========");

            try
            {
                var sett = ScanningSettings.Current;
                sett.MinQuality = 50;
                sett.MinMinutiaCount = 30;
                sett.MinArea = 0;// Int32.Parse(ConfigurationManager.AppSettings["MinArea"].ToString());
                sett.MaxRotation = 30;
                sett.CaptureDelay = 300;
                sett.ShowGauges = true;

                sett.Futronic.ScanDose = 0;
                sett.Futronic.EliminateBackground = true;

                var finger = new Finger(FingerIndex.LeftThumb);
                var fingerScanForm = new FingerScanForm(finger);
                var showDialog = fingerScanForm.ShowDialog();
                if (showDialog != null && showDialog.Value)
                {
                    
                    var wsqConverter = new Afisteam.Biometrics.Math.Innovatrics.InnovatricsImageConverter();
                    var wsq = wsqConverter.ConvertTo(finger.BestFingerprint.GetImageAsArray(), ImgFormat.WSQ);

                    // Save to server
                    using (var client = new HttpClient())
                    {
                        //Passing service base url  
                        var serviceurl = ConfigurationManager.AppSettings["serviceurl"];
                        var contracturl = serviceurl + "/api/SaveFingerprint";
                        client.BaseAddress = new Uri(serviceurl);

                        var response = client.PostAsync(contracturl, new StringContent(
                            new JavaScriptSerializer().Serialize(wsq), Encoding.UTF8, "application/json")).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            sb.Append("Failed to save image to server via service");
                            sb.Append("");

                            if (File.Exists(location))
                            {
                                File.AppendAllText(location, sb.ToString());
                                sb.Clear();
                            }
                            else
                            {
                                File.WriteAllText(location, sb.ToString());
                            }
                        }
                    }
                }
                else
                {
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {                
                sb.Append(ex.Message);
                sb.Append("");

                if (File.Exists(location))
                {
                    File.AppendAllText(location, sb.ToString());
                    sb.Clear();
                }
                else
                {
                    File.WriteAllText(location, sb.ToString());
                }
            }
        }

        #endregion
    }
}
