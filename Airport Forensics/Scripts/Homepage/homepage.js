﻿$(document).ready(function () {
    $('.forgottenpw').click(function () {
        $('#ForgotPassword').lightbox_me({
            centered: true
        });
    });

    $('.register').click(function () {
        $('#registrationDiv').lightbox_me({
            centered: true
        });
    });
});

function SendReset() {
    var email = $('#txtResetEmail').val();
    if (email == '') {
        $('#forgotpwval').text('Please enter your email address');
    }
    else {
        $('#floadingdiv').slideDown(function () {
            $.ajax({
                type: "GET",
                url: "/ResetPassword",
                data: { email: email },
                dataType: "json",
                error: function (result) {
                    $('#floadingdiv').slideUp(function () {
                        $('#ForgotPassword').trigger('close');
                    });
                },
                success: function (result) {
                    $('#floadingdiv').slideUp(function () {
                        $('#ForgotPassword').trigger('close');
                        setTimeout(function () {
                            if (result.status == "0")
                                showPopup("An email has been sent to you Iris mailbox giving details on how to reset your password.");

                            if (result.status == "1")
                                showPopup("<div>An email has already been sent to your mailbox giving details on how to reset your password.</div>");

                            if (result.status == "2")
                                showPopup("<div>This email did not match any of our registered users.</div>");

                            if (result.status == "5")
                                showPopup("<div>" + result.message + "</div>");
                        }, 500);
                    });
                }
            });
        });
    }
}


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
