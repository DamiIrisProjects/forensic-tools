﻿var showing = 0;

$(document).ready(function () {
    var popup = $('#popup').val();

    if (popup != "") {
        $('#PopupInfo').lightbox_me({
            centered: true
        });
        $('#popup').val("");
    }

    $('#CloseTagPop').click(function (e) {
        $(this).fadeOut(function () { $(this).trigger('close'); });
    });

    // Set which option is active
    $('.option').removeClass('optionactive');
    $('#apprepoption').addClass('optionactive');
    
    $('#selectoptions2').select2({
        theme: "classic",
        placeholder: "Select report to print"
    });

    $(".jdpicker").datepicker({
        yearRange: "-20:+0",
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true
    });  

    $("#PassportcmbReportType").select2({
        placeholder: "Select report to print...",
        minimumResultsForSearch: 8
    });
});

function PassportReportPrint() {
    if ($('.printbutton').css('opacity') == '0.2')
        return;

    var searchtype = $("#selectoptions2").val();

    if (searchtype == '') {
        ShowPopup("Please select what report you would like to print");
        return false;
    }

    var from = $('#PassporttxtFromDatex').val();
    var to = $('#PassporttxtToDatex').val();

    $('#inforesult').text('');
    $('.printbutton').css('opacity', '0.2');
    $('.printinfo').fadeIn(function () {
        var intval = LoadingText('infomsg', 'Generating Report', 15, 700);
        $.ajax({
            type: "GET",
            url: "/report/GenerateReport",
            data: { searchtype: searchtype, from: from, to: to },
            dataType: "json",
            error: function (result) {
                clearInterval(intval);
                $('#infomsg').text('Generating Report . . . . .');
                $('#inforesult').css('color', 'red');
                $('#inforesult').text('Failed: ' + result.errormsg);
                $('.printbutton').css('opacity', '1');
            },
            success: function (result) {
                $('.printbutton').css('opacity', '1');
                clearInterval(intval);
                $('#infomsg').text('Generating Report . . . . .');

                if (result.status == 1) {
                    $('#inforesult').css('color', 'green');
                    $('#inforesult').text('Success. Report sent to printer.');
                    $('#branchReportDiv').html(result.page);
                    window.print();

                    if (window.stop) {
                        location.reload(); //triggering unload (e.g. reloading the page) makes the print dialog appear
                        window.stop(); //immediately stop reloading
                    }
                }

                else if (result.status == 0) {
                    $('#inforesult').css('color', 'red');
                    $('#inforesult').text('Denied: You do not have access rights to print this report.');
                }

                else if (result.status == 3) {
                    window.location.href = '/';
                }

                else {
                    $('#inforesult').css('color', 'red');
                    $('#inforesult').text('Failed: ' + result.errormsg);
                }
            }
        });
    });
}

function LoadingText(span, text, maxdots, intvl)
{
    var span = document.getElementById(span);


    var int = setInterval(function () {
        if ((span.innerHTML += ' . ').length > (text.length + maxdots)) {
            span.innerHTML = text;
        }
        //clearInterval( int ); // at some point, clear the setInterval
    }, intvl);

    return int;
}

function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}
