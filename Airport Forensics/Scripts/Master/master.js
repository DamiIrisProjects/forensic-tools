﻿$(document).ready(function () {
    // Turn off all cacheing for ajax
    $.ajaxSetup({ cache: false });

    // application wide functions
    $(document).on('click', '.closeme, .closebtn', function () {
        $('.PopupWindow').fadeOut(function () { $(this).trigger('close') });
    });

    // Change themes
    $(document).on('click', '.switchtheme', function () {
        $('body').fadeOut(function () {
            if ($(this).hasClass('pptheme'))
                $(this).removeClass('pptheme');
            else
                $(this).addClass('pptheme');

            $(this).fadeIn();
        });

        $("#menu li.menuitem").find("div.outersub").fadeOut(100);
    });

    // Custom function for Mina: hide qc details
    $(document).on('click', '.toggleqc', function () {
        if ($(this).text() === "Hide Audit") {
            $('.qcdiv').fadeOut();
            $('#toggleqc').val(0);
            $(this).text('Show Audit');
        }
        else {
            $('.qcdiv').fadeIn();
            $('#toggleqc').val(1);
            $(this).text('Hide Audit');
        }
    });

    $(document).on('click', 'input[type=submit]', function () {
        if ($(this).parents('form:first').valid())
            $(this).siblings('.loadingdiv').addClass("loading");
    });

    var ajaxUpdate = function (form) {
        var $form = form;

        if ($form.valid()) {
            var options = {
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize(),
                cache: false,
            };

            $.ajax(options).done(function (data) {
                var $target = $('#' + $form.attr("data-target"));
                var successmsg = $form.attr("data-success");

                if (data.err !== undefined) {
                    //$form.find('.errormsg').html(data.err);
                    $form.parent().fadeOut(function () {
                        showPopup(data.err);
                    });
                }
                else {
                    if (successmsg !== undefined) {
                        // If its a delete, remove that row
                        if (successmsg.indexOf("removed") > -1)
                            $form.parent().fadeOut();
                        else
                            showPopup(successmsg);
                    }
                }

                $('.loadingdiv').removeClass("loading");
            }).error(function (data) {
                $form.parent().fadeOut(function () {
                    showPopup('Could not connect to server');
                    $('.loadingdiv').removeClass("loading");
                });
            });
        }
        else
            $('.loadingdiv').removeClass("loading");

        return false;
    };

    var ajaxPartialUpdate = function (form) {
        var $form = form;

        if ($form.valid()) {
            var options = {
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize(),
                cache: false,
            };

            $.ajax(options).done(function (data, jqxhr) {
                if (jqxhr.status === 401 || jqxhr.status === 403) {
                    window.location.href = '/';
                    return;
                }

                var $target = $('#' + $form.attr("data-target"));

                var investigatorsearch = $form.attr("data-investigator-search");

                if (data.err !== undefined)
                    $form.find('.errormsg').text(data.err);
                else
                {
                    $target.fadeOut(function () {
                        $(this).html(data.page);

                        // if its investigator 
                        if (investigatorsearch) {
                            if (data.num !== 0) {
                                $('.searchresultintro').removeClass('searchresultintrozero');

                                //Hide search box                        
                                $('#searchcriteria').fadeOut(function () {
                                    $target.fadeIn();
                                    $('#searchagain').fadeIn();
                                })
                            }
                            else
                            {
                                $('.searchresultintro').addClass('searchresultintrozero');
                                $target.fadeIn();
                            }
                        }
                    });                    
                }

                $('.loadingdiv').removeClass("loading");
            }).error(function (e, xhr, settings) {
                showPopup('An error occurred processing your request. It has been logged.');
                $('.loadingdiv').removeClass("loading");
            })
        }
        else
            $('.loadingdiv').removeClass("loading");
    };

    var ajaxInsert = function (form) {
        var $form = form;

        if ($form.valid()) {
            var options = {
                url: $form.attr("action"),
                type: $form.attr("method"),
                data: $form.serialize(),
                cache: false,
            };

            $.ajax(options).done(function (data) {
                if (data.err !== undefined)
                    $form.find('.apperror').text(data.err).slideDown();
                else {
                    var successmsg = $($form.attr("data-success"));
                    $form.find('input[type="text"]').each(function () {
                        $(this).val('');
                    });
                    $form.find('.apperror').text('').slideUp();
                }

                $('.loadingdiv').removeClass("loading");
            });

            $.ajax(options).error(function (data) {
                $('.loadingdiv').removeClass("loading");
            });
        }

        return false;
    };

    $(document).on('submit', "form[data-partial='true']", function (e) {
        e.preventDefault();
        ajaxPartialUpdate($(this));
    });

    $(document).on('submit', "form[data-insert='true']", function (e) {
        e.preventDefault();
        ajaxInsert($(this));
    });

    $(document).on('submit', "form[data-update='true'] , form[data-del='true']", function (e) {
        e.preventDefault();
        ajaxUpdate($(this));
    });

    // login stuff
    $('.outersub').css("min-width", $('#divLoggedIn').width() + "px").css("left", "-" + ($('#divLoggedIn').width() - 107 + 77) + "px");

    $(document).on('click', '#menu li.menuitem', function () {
        if ($(".outersub").is(':animated'))
            return;

        $(this).find("div.outersub").hide().fadeIn();
    });

    $(document).mouseup(function (e) {
        $(".outersub").fadeOut(100);
        //var container = $("#menu li.menuitem");
        //container.find("div.outersub").fadeOut(100);

        //if (!container.is(e.target) // if the target of the click isn't the container...
        //    && container.has(e.target).length === 0) // ... nor a descendant of the container
        //{
        //    container.find("div.outersub").fadeOut(100);
        //}
    });

    // Logged out errors
    $(document).ajaxError(function (event, jqxhr, settings, exception) {
        if (jqxhr.status === 401 || jqxhr.status === 403 ) {
            window.location.href = '/';
        }
    });

    $(document).ajaxSuccess(function (event, jqxhr, settings, exception) {
        if (jqxhr.getResponseHeader("Content-Type").toLowerCase().indexOf("text/html") >= 0) {
            window.location = '/';
        }
    });

    // Messages
    if ($('#Msg').val() !== '')
        showPopup($('#Msg').val());
});

function showPopup(msg) {
    $('#popupmsgInner').html(msg);
    $('#PopupMsg').lightbox_me({
        centered: true
    });
}
