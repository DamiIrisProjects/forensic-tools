﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Airport_Forensics.Helpers
{
    public class IsRequired : ValidationAttribute
    {
        public string Value { get; set; }
        public string Default { get; set; }
        public override bool IsValid(object value)
        {
            if (value == null || value.ToString() == string.Empty || Value == Default)
                return false;

            return true;
        }
    }
}