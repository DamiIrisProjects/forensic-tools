﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Airport_Forensics.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Please enter Firstname")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter Surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Please enter PhoneNumber")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please enter Email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please confirm Email")]
        [CompareAttribute("Email", ErrorMessage = "Confirmation email does not match")]
        public string ConfirmEmail { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm Password")]
        [DataType(DataType.Password)]
        [CompareAttribute("Password", ErrorMessage = "Confirmation password does not match")]
        public string ConfirmPassword { get; set; }

        public string Guid { get; set; }
    }
}