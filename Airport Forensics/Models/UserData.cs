﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airport_Forensics.Models
{
    public class UserData
    {
        public string Firstname { get; set; }

        public string Surname { get; set; }

        public int Id { get; set; }
    }
}