﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute("AirportForensics", typeof(Airport_Forensics.Startup))]
namespace Airport_Forensics
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
