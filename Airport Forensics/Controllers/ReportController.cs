﻿using Airport_Forensics.Helpers;
using EPMS_Forensics_Entities;
using EPMS_Forensics_Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airport_Forensics.Controllers
{
    [UserAuthorized]
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            // This should only be accessible by the CGI
            if(Session["CurrentUser"] != null && ((Session["CurrentUser"] as EPMS_Forensics_Entities.Person).PersonId != 228 && (Session["CurrentUser"] as EPMS_Forensics_Entities.Person).PersonId != 68))
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult GenerateReport(string searchtype, string from, string to)
        {
            // This should only be accessible by the CGI
            if (Session["CurrentUser"] != null && ((Session["CurrentUser"] as EPMS_Forensics_Entities.Person).PersonId != 228 && (Session["CurrentUser"] as EPMS_Forensics_Entities.Person).PersonId != 68))
                return Json(new { status = 2, errormsg = "You do not have access to this" }, JsonRequestBehavior.AllowGet);

            try
            {
                // Get data for the interval
                DateTime From = string.IsNullOrEmpty(from) ? DateTime.Today : DateTime.ParseExact(from, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(to) ? DateTime.Today : DateTime.ParseExact(to, "dd/MM/yyyy", null);

                List<PassportReport> data = new InvestigatorProxy().InvestigatorChannel.GetPassportsIssued(From, To);

                // Calculate details
                BranchReport report = new BranchReport();
                report.Page = 1;
                report.ReportName = "Production Report";
                report.ReportSubHeader = "OVERALL PASSPORTS ISSUANCE BY TYPE";
                report.Interval = "( BETWEEN " + From.ToString("dd-MMM-yyyy") + " AND " + To.ToString("dd-MMM-yyyy") + " )";
                report.TimeStamp = DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm tt");
                report.SubInterval = "From " + From.ToString("dd-MMM-yyyy") + " to " + To.ToString("dd-MMM-yyyy");

                report.OfficialPassports = new Dictionary<string, int>();
                report.OrdinaryPassports = new Dictionary<string, int>();
                report.DiplomaticPassports = new Dictionary<string, int>();

                if (searchtype == "0")
                {
                    report.HeaderName = "ALL BRANCHES (TOTAL BOOKLETS ISSUED)";
                }

                if (searchtype == "1")
                {
                    report.HeaderName = "LOCAL BRANCHES (TOTAL BOOKLETS ISSUED)";
                    report.ReportSubHeader = "LOCAL PASSPORTS ISSUANCE BY TYPE";
                }

                if (searchtype == "2")
                {
                    report.HeaderName = "FOREIGN BRANCHES (TOTAL BOOKLETS ISSUED)";
                    report.ReportSubHeader = "FOREIGN PASSPORTS ISSUANCE BY TYPE";
                }

                foreach (PassportReport rep in data)
                {
                    if (searchtype == "1")
                    {
                        // Local branches
                        if (rep.IsForeign == 1)
                            continue;
                    }

                    if (searchtype == "2")
                    {
                        // Foreign branches
                        if (rep.IsForeign == 0)
                            continue;
                    }

                    if (rep.DocType == "P")
                    {
                        report.OrdinaryPassports.Add(rep.BranchName, rep.NumberIssued);
                    }

                    if (rep.DocType == "PF")
                    {
                        report.OfficialPassports.Add(rep.BranchName, rep.NumberIssued);
                    }

                    if (rep.DocType == "PD")
                    {
                        report.DiplomaticPassports.Add(rep.BranchName, rep.NumberIssued);
                    }
                }

                report.DiplomaticPassportsTotal = report.DiplomaticPassports.Sum(x => x.Value);
                report.OrdinaryPassportsTotal = report.OrdinaryPassports.Sum(x => x.Value);
                report.OfficialPassportsTotal = report.OfficialPassports.Sum(x => x.Value);

                return Json(new { status = 1, page = iHelper.JsonPartialView(this, "_BranchReport", report) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 2, errormsg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}