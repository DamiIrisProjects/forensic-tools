﻿using EPMS_Forensics_Entities;
using EPMS_Forensics_Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Airport_Forensics.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string guid, string reset)
        {
            try
            {
                if (!string.IsNullOrEmpty(guid))
                {
                    int result = new AirportLoginProxy().LoginChannel.ActivateUser(guid);

                    if (result == 1)
                    {
                        ViewBag.Msg = "Account successfully activated. An email has been sent to the user to confirm the activation";
                    }
                }

                if (!string.IsNullOrEmpty(reset))
                {
                    int result = new AirportLoginProxy().LoginChannel.VerifyPasswordChange(reset);

                    if (result == 0)
                    {
                        ViewBag.ShowUpdatePassword = 1;
                        ViewBag.Guid = reset;
                        //ViewBag.Msg = "Password has been updated. Please sign in with new password";
                    }

                    if (result == 1)
                        ViewBag.Msg = "Invalid request";

                    if (result == 2)
                        ViewBag.Msg = "<div style='margin-bottom: 10px'>Your request to change password has expired. Please do another request.</div>";
                }
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
            }

            return View();
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}