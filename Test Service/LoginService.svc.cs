﻿using EPMS_Forensics_Contract;
using EPMS_Forensics_Data;
using EPMS_Forensics_Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EPMS_Forensics_Service
{
    public class LoginService : ILogin
    {
        public Person LoginUser(string username, string password)
        {
            LoginData logindata = new LoginData();
            AuditData auditdata = new AuditData();

            // Get user details
            Person user = logindata.GetUserDetails(username);

            if (user != null)
            {
                Audit audit = new Audit()
                {
                    TimeStamp = DateTime.Now,
                    UserId = user.PersonId
                };

                // Verify password
                SaltedHash sh = SaltedHash.Create(user.Salt, user.Hash);

                if (sh.Verify(password))
                {
                    //Login success audit
                    audit.EventType = (int)AuditEnum.LoginSuccess;
                    auditdata.AddAudit(audit);
                    return user;
                }
                else
                {
                    //Login failed audit
                    audit.EventType = (int)AuditEnum.LoginFailed;
                    auditdata.AddAudit(audit);
                }
            }

            return null;
        }

        public int RegisterUser(string firstname, string surname, string email, string phonenumber, string password)
        {
            LoginData data = new LoginData();
            int result = data.VerifyUsernameDoesNotExists(email);

            if (result == 0)
            {
                return data.RegisterUser(firstname, surname, phonenumber, email, password);
            }

            return result;
        }

        public int ResetUserPassword(string username)
        {
            return new LoginData().ResetUserPassword(username);
        }

        public int ActivateUser(string email)
        {
            return new LoginData().ActivateUser(email);
        }

        public int VerifyPasswordChange(string email)
        {
            return new LoginData().VerifyPasswordChange(email);
        }

        public int UpdateForgottenPassword(string password, string guid)
        {
            return new LoginData().UpdateForgottenPassword(password, guid);
        }
    }
}
