﻿using EPMS_Forensics_Contract;
using EPMS_Forensics_Data;
using EPMS_Forensics_Entities;

namespace EPMS_Forensics_Service
{
    public class ErrorService : IError
    {
        public void SaveError(Error error)
        {
            new ErrorData().SaveError(error);
        }
    }
}
