﻿using EPMS_Forensics_Contract;
using EPMS_Forensics_Data;
using EPMS_Forensics_Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EPMS_Forensics_Service
{
    public class AuditService : IAudit
    {
        public void AddAudit(Audit audit)
        {
            new AuditData().AddAudit(audit);
        }
    }
}
