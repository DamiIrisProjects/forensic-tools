﻿using System;
using System.IO;
using System.Net;
using System.Web.Mvc;

namespace EPMS_Forensics.Helpers
{
    public static class Helper
    {
        public static string ToFirstLetterUpperAllWords(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var words = input.ToLower().Split(' ');
                var result = string.Empty;

                foreach (var word in words)
                {
                    if (word != string.Empty && word != " ")
                    {
                        var a = word.ToCharArray();
                        a[0] = char.ToUpper(a[0]);

                        if (result == string.Empty)
                            result = result + new string(a);
                        else
                            result = result + " " + new string(a);
                    }
                }

                return result;
            }

            return string.Empty;
        }

        public static string ToFirstLetterUpper(string word)
        {
            if (word != string.Empty && word != " ")
            {
                var a = word.ToLower().ToCharArray();
                a[0] = char.ToUpper(a[0]);

                return new string(a);
            }

            return string.Empty;
        }

        public static string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }

        public static string Ordinal(int number)
        {
            string suffix;

            var ones = number % 10;
            var tens = (int)Math.Floor(number / 10M) % 10;

            if (tens == 1)
            {
                suffix = "th";
            }
            else
            {
                switch (ones)
                {
                    case 1:
                        suffix = "st";
                        break;

                    case 2:
                        suffix = "nd";
                        break;

                    case 3:
                        suffix = "rd";
                        break;

                    default:
                        suffix = "th";
                        break;
                }
            }
            return $"{number}{suffix}";
        }
    }

    public class BetterWebClient : WebClient
    {
        private WebRequest _request;

        public BetterWebClient(CookieContainer cookies, bool autoRedirect = true)
        {
            CookieContainer = cookies ?? new CookieContainer();
            AutoRedirect = autoRedirect;
        }

        //Gets or sets whether to automatically follow a redirect
        public bool AutoRedirect { get; set; }

        //Gets or sets the cookie container, contains all the 
        //cookies for all the requests
        public CookieContainer CookieContainer { get; set; }

        //Gets last cookie header
        public string Cookies => GetHeaderValue("Set-Cookie");

        //Get last location header
        public string Location => GetHeaderValue("Location");

        //Get last status code
        public HttpStatusCode StatusCode
        {
            get
            {
                var result = HttpStatusCode.BadRequest;

                if (_request != null)
                {
                    var response = GetWebResponse(_request) as HttpWebResponse;

                    if (response != null)
                    {
                        result = response.StatusCode;
                    }
                }

                return result;
            }
        }

        public string GetHeaderValue(string headerName)
        {
            string result = null;

            if (_request != null)
            {
                var response = GetWebResponse(_request) as HttpWebResponse;
                if (response != null)
                {
                    result = response.Headers[headerName];
                }
            }

            return result;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            _request = base.GetWebRequest(address);

            var httpRequest = _request as HttpWebRequest;

            if (httpRequest != null)
            {
                httpRequest.AllowAutoRedirect = AutoRedirect;
                httpRequest.CookieContainer = CookieContainer;
                httpRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            }

            return _request;
        }
    }
}