﻿using EPMS_Forensics.Models;
using EPMS_Forensics_Entities;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace EPMS_Forensics.Helpers
{
    public class UserAuthorized : AuthorizeAttribute
    {
        // Custom property
        public string AccessLevel { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthorized = base.AuthorizeCore(httpContext);

            // Recreate session details
            if (httpContext.Session["CurrentUser"] == null)
            {
                //Extract the forms authentication cookie
                var authCookie = httpContext.Request.Cookies["IrisDashboardUser"];

                if (authCookie != null)
                {
                    var authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                    if (!string.IsNullOrEmpty(authTicket.UserData))
                    {
                        var userdata = new JavaScriptSerializer().Deserialize<UserData>(authTicket.UserData);
                        httpContext.Session["CurrentUser"] = new Person() { PersonId = userdata.Id, Firstname = userdata.Firstname, Surname = userdata.Surname };
                    }
                }
            }

            if (!isAuthorized)
            {
                return false;
            }

            // Ensure that User details are in session, otherwise mark as unauthorized
            if (httpContext.Session["CurrentUser"] == null)
            {
                FormsAuthentication.SignOut();
                return false;
            }

            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }
    }
}