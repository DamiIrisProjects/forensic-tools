﻿using System.Web.Mvc;
using System.Web.Routing;

namespace EPMS_Forensics
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "Account", // Route name
               "Account/{action}/{id}", // URL with parameters
               new { controller = "Account", action = "Login", id = UrlParameter.Optional } // Parameter defaults
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
