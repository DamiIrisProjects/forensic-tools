﻿$(document).ready(function () {
    // Turn off all cacheing for ajax
    $.ajaxSetup({ cache: false });

    // On print
    $(document).on('click', '.printone', function () {
        $('.tableparentmargin').removeClass('notselected');
        $(".tableparentmargin").not(":visible").addClass('notselected');
        $('.tableparentmargin:visible .printlink').click();
    });

    $(document).on('click', '.printall', function () {
        $('.tableparentmargin').removeClass('notselected').addClass('selected');
        $('.tableparentmargin:visible .printlink').click();
    });

    // Set which option is active
    $('.option').removeClass('optionactive');
    $('#appinvoption').addClass('optionactive');

    // activate select2 on select boxes
    $('#selectoptions').select2({
        theme: "classic",
        placeholder: "Select a search option"
    }).change(function () {
        // Show the right options depending on which is selected
        var divname = $(this).val();
        $.when($('.searchoption').slideUp()).done(function(){
            $('#' + divname).slideDown();
        });
    }).on('select2:unselecting', function () {
        $(this).data('unselecting', true);
    }).on('select2:opening', function (e) {
        if ($(this).data('unselecting')) {
            $(this).removeData('unselecting');
            e.preventDefault();
        }
    });;

    $('#stateoforigin').select2({
        theme: "classic",
        allowClear: true,
        placeholder: "Select a state"
    }).change(function () {
        // Update variable
        $('#state').val($(this).val());
    }).on('select2:unselecting', function () {
        $(this).data('unselecting', true);
    }).on('select2:opening', function (e) {
        if ($(this).data('unselecting')) {
            $(this).removeData('unselecting');
            e.preventDefault();
        }
    });;

    $('#selectgender').select2({
        theme: "classic",
        allowClear: true,
        placeholder: "Select gender"
    }).change(function () {
        // Update variable
        $('#sex').val($(this).val());
    }).on('select2:unselecting', function () {
        $(this).data('unselecting', true);
    }).on('select2:opening', function (e) {
        if ($(this).data('unselecting')) {
            $(this).removeData('unselecting');
            e.preventDefault();
        }
    });;
    
    // Handle selection of branch. Had to do it with this long roundabout way since update to select2 ~_~
    var changing = 0;

    $('#branchIds').select2({
        theme: "classic",
        placeholder: "Code",
        width: 86
    }).change(function () {
        var id = $('#branchIds').val();
        $('#branchid').val(id);
        // prevent infinite loop of 'change'
        if (changing == 1)
            changing = 0;
        else {
            changing = 1;
            $('#branchNames').val(id).trigger("change");
            setTimeout(function () {
                $("#enrolmentid").focus();
            }, 0);
        }
    }); 

    $('#branchNames').select2({
        theme: "classic",
        placeholder: "Branch name",
        width: 160,
    }).change(function () {
        var id = $('#branchNames').val();
        $('#branchid').val(id);

        // prevent infinite loop of 'change'
        if (changing == 1)
            changing = 0;
        else {
            changing = 1;
            $('#branchIds').val(id).trigger("change");
            setTimeout(function () {
                $("#enrolmentid").focus();
            }, 0);
        }
    });

    // Setup calenders
    $(".jdpicker").datepicker({
        yearRange: "-120:+0",
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true
    });

    // Clear search criteria
    $(document).on('click', '.clearbtn', function () {
        var $form = $('#searchbymember').find('form');
        $form.trigger("reset");
        $form.find('.apperror').text('');
        $form.find('.errormsg').text('');
        $('.selectbox').select2("val", "");
        $form.find('input[type="text"], input[type="number"]').each(function () {
            $(this).val('').removeClass('disabled');
        });
    });

    // when search again button is clicked, show search options
    $(document).on('click', '.searchagainbtn', function () {
        $('#SearchResults, #SelectedDetails').fadeOut();
        $('#searchagain').fadeOut(function () { $('#searchcriteria').fadeIn(); });

        // hide options button
        $('.showoptions').fadeOut();
    });

    // when previous search results button is clicked, show previous results
    $(document).on('click', '.prevsearchresults', function () {
        $('#SelectedDetails').fadeOut(function () {
            $('#SearchResults').fadeIn();
        });

        // hide options button
        $('.showoptions').fadeOut();
    });

    // Show options to search for
    $(document).on('click', '.applicantselection', function () {
        $('#selectedid').val($(this).attr('id'));
        $('#selectedformnos').val($(this).find('#formno').val());
        $('.applicantselected').text($(this).find('.applicantinfo').text());
        $('#searchFunctions').lightbox_me({
            centered: true
        });
    });

    $(document).on('click', '#showoptions', function () {
        $('#searchFunctions').lightbox_me({
            centered: true
        });
    });

    // Collect fingerprint
    $(document).on('click', '.collectfingerbtn', function () {
        CollectFingerprint();
    });

    $(document).on('click', '.searchfingerbtn', function () {
        $('#fileInput').click();
    });

    $(document).on('submit', "#uploader", function (e) {
        SearchForFingerprint();
    });    

    // Only allow one type of application option (as it should be unique)
    $('#searchbyapplication .textbox').on('input propertychange paste', function () {
        if ($(this).val() == '') {
            $('#searchbyapplication .textbox').each(function () {
                $(this).removeClass('disabled');
            });
        }
    });

    // previous and next buttons
    $(document).on('click', '.prev', function () {
        if (!$(this).hasClass('disabled')) {
            var count = $('#totalpassports').val();
            var currid = $('#currentpassport').val();
            currid--;

            // update currid variable
            $('#currentpassport').val(currid);

            // update the count 
            $('.nextprevdiv').fadeOut(function () {
                $('.countinfo').text((currid) + ' of ' + count);

                // disable the next button if its at max
                if (currid == 1)
                    $('.prev').addClass('disabled');

                // enable next btn
                if ($('.next').hasClass('disabled'))
                    $('.next').removeClass('disabled')

                $('.nextprevdiv').fadeIn();
            });

            // hide the current one and show the next one
            $('#application' + (currid + 1)).fadeOut(function () {
                $('#application' + currid).fadeIn();
            });


        }
    });

    $(document).on('click', '.next', function () {
        if (!$(this).hasClass('disabled')) {    
            var count = $('#totalpassports').val();
            var currid = $('#currentpassport').val();
            currid++;

            // update currid variable
            $('#currentpassport').val(currid);

            // update the count 
            $('.nextprevdiv').fadeOut(function () {
                $('.countinfo').text((currid) + ' of ' + count);

                // disable the next button if its at max
                if (currid == count)
                    $('.next').addClass('disabled');

                // enable prev btn
                if ($('.prev').hasClass('disabled'))
                    $('.prev').removeClass('disabled')

                $('.nextprevdiv').fadeIn();
            });

            // hide the current one and show the next one
            $('#application' + (currid - 1)).fadeOut(function () {
                $('#application' + currid).fadeIn();
            });                        
        }
    });
});

function CollectFingerprint() {
    $('#fingerprintload').addClass('loading');

    try {
        var shell = new ActiveXObject("WScript.shell");
        var result = shell.run("file:///C:/Biometrics/CollectBiometrics.exe " + 1, 5, true);

        // code to catch window's event and keep focus on windows app, like modal simulation
        window.onfocusin = function () {
            //shell.AppActivate();
        }

        // sort out window focusing otherwise you end up clicking twice to do anything
        window.blur();
        window.focus();

        if (result == 1) {
            // Do something with fingerprint
            $('.searchingafis').fadeIn();
            $.ajax({
                type: "GET",
                url: '/Investigator/SearchByFingerprint',
                dataType: "json",
                error: function (result) {
                    $('#fingerprintload').removeClass('loading');
                    $('.searchingafis').hide();
                    showPopup("Could not connect to database. Please ensure you are still connected to the network.");
                },
                success: function (result) {
                    $('#fingerprintload').removeClass('loading');
                    $('.searchingafis').hide();

                    if (result.status == 1) {
                        if (result.num != 0) {
                            //Hide search box    
                            $('.searchresultintro').removeClass('searchresultintrozero');

                            //Hide search box                        
                            $('#searchcriteria').fadeOut(function () {
                                $('#SearchResults').html(result.page).fadeIn();
                                $('#searchagain').fadeIn();
                            })
                        }                        
                    }
                    else {
                        showPopup(result.err);
                    }
                }
            });
        }
        else
        {
            $('#fingerprintload').removeClass('loading');
        }
    }
    catch (err) {
        $('#fingerprintload').removeClass('loading');
        if (err.message == 'ActiveXObject is not defined') {
            showPopup("You browser is not set to allow Activex.");
        }
    }
}

function SearchForFingerprint() {
    $('#fingerprintload').addClass('loading');
    $('.searchingafis').fadeIn();

    try {
        var data = new FormData();
        var file = document.getElementById("fileInput").files[0];
        data.append("UploadedFile", file);

        $.ajax({
            type: 'POST',
            url: "/Investigator/UploadFingeprint",
            contentType: false,
            processData: false,
            data: data,
            error: function (result) {
                $('#fingerprintload').removeClass('loading');
                showPopup("Error uploading fingerprint");
            },
            success: function (result) {
                $('#fingerprintload').removeClass('loading');
                $('.searchingafis').hide();

                if (result.status == 1) {
                    if (result.num != 0) {
                        //Hide search box    
                        $('.searchresultintro').removeClass('searchresultintrozero');

                        //Hide search box                        
                        $('#searchcriteria').fadeOut(function () {
                            $('#SearchResults').html(result.page).fadeIn();
                            $('#searchagain').fadeIn();
                        })
                    }
                }
                else {
                    showPopup(result.err);
                }
            }
        });
    }
    catch (err) {
        $('#fingerprintload').removeClass('loading');
        $('.searchingafis').hide();
        if (err.message == 'ActiveXObject is not defined') {
            showPopup("You browser is not set to allow Activex.");
        }
    }
}

function RefreshApplicationDetails(id)
{
    $('#selectedid').val(id);
    DoApplicantFunction(5, 1);
}

function DoApplicantFunction(type, isrefresh)
{
    var selectedid = $('#selectedid').val();
    var selectedformnos = $('#selectedformnos').val();
    var loading, url;
    if (isrefresh == 1)
        loading = '#refreshloading';
    else
        loading = '#searchfunctionloading';

    $(loading).addClass('loading');

    if (type == 1)
    {
        url = '/Investigator/ViewSummary';
    }

    if (type == 2) {
        url = '/Investigator/ViewAfisTransaction';
    }

    if (type == 3) {
        url = '/Investigator/ViewEnrolmentHistory';
    }

    if (type == 4) {
        url = '/Investigator/GetBookletInfo';
    }

    if (type == 5) {
        url = '/Investigator/GetApplicationInfo';
    }

    $.ajax({
        type: "POST",
        url: url,
        data: { id: selectedid, formnos: selectedformnos },
        dataType: "json",
        error: function (result) {
            $(loading).removeClass('loading');
            showPopup("Could not connect to database. Please ensure you are still connected to the network.");
        },
        success: function (result) {
            $(loading).removeClass('loading');
            $('.closebtn').click();

            if (result.status == 1) {
                $('#SearchResults').fadeOut(function () {
                    $('#SelectedDetails').html(result.page);

                    // For custom case with Mina
                    if (url == '/Investigator/GetApplicationInfo')
                    {
                        if ($('#toggleqc').val() == '0') {
                            $('.qcdiv').hide();
                        }
                    }

                    $('#SelectedDetails').fadeIn();
                    if ($('#searchagain').css('display') == 'none')
                        $('#searchagain').fadeIn();
                });
            }
            else {
                showPopup(result.err);
            }
        }
    });

    // Show options again
    $('.showoptions').fadeIn();
}