﻿namespace EPMS_Forensics.Models
{
    public class UserData
    {
        public string Firstname { get; set; }

        public string Surname { get; set; }

        public int Id { get; set; }
    }
}