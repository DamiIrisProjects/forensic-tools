﻿using EPMS_Forensics_Entities;
using System.Collections.Generic;

namespace EPMS_Forensics.Models.Investigator
{
    public class SearchResults
    {
        public SearchResults()
        {
            Applicants = new List<Person>();
        }

        public List<Person> Applicants { get; set; }
    }
}