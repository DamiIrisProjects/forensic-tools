﻿using System.ComponentModel.DataAnnotations;

namespace EPMS_Forensics.Models
{
    public class ResetPassword
    {
        [Required(ErrorMessage = "Please enter Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm Password")]
        [DataType(DataType.Password)]
        [CompareAttribute("Password", ErrorMessage = "Confirmation password does not match")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Invalid transaction code")]
        public string Guid { get; set; }
    }
}