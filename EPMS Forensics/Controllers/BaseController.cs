﻿using EPMS_Forensics_Entities;
using EPMS_Forensics_Proxy;
using System;
using System.Web.Mvc;

namespace EPMS_Forensics.Controllers
{
    public class BaseController: Controller
    {
        public void SaveError(Exception ex)
        {
            new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
        }
    }
}