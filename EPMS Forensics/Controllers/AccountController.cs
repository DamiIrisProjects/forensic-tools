﻿using EPMS_Forensics.Models;
using EPMS_Forensics_Entities;
using EPMS_Forensics_Proxy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace EPMS_Forensics.Controllers
{
    public class AccountController : BaseController
    {
        //[HandleAntiForgeryError]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var proxy = new LoginProxy();
                    var user = proxy.LoginChannel.LoginUser(model.Email, model.Password);

                    if (user == null)
                    {
                        ModelState.AddModelError("", "Invalid Username or Password");
                        return View();
                    }

                    Session["CurrentUser"] = user;

                    if (model.Email == "irisdash@irissmart.com")
                    {
                        var cookie = FormsAuthentication.GetAuthCookie(user.FullName, true);
                        cookie.Name = "IrisDashboardUser";
                        cookie.Expires = DateTime.Now.AddMonths(12);
                        var ticket = FormsAuthentication.Decrypt(cookie.Value);
                        var userData = new UserData { Firstname = user.Firstname, Surname = user.Surname, Id = user.PersonId };
                        if (ticket != null)
                        {
                            var newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name,
                                ticket.IssueDate, ticket.Expiration, ticket.IsPersistent, new JavaScriptSerializer().Serialize(userData));
                            cookie.Value = FormsAuthentication.Encrypt(newTicket);
                        }
                        Response.Cookies.Add(cookie);
                    }
                    else
                        FormsAuthentication.SetAuthCookie(user.FullName, false);

                    return RedirectToAction("Index", "Home");
                }
                catch (Exception ex)
                {
                    SaveError(ex);
                    return View("Error");
                }
            }

            return RedirectToAction("Index", "Home", new { id = "Retry" });
        }

        [Authorize]
        [HttpPost]
        [HandleAntiForgeryError]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            // Remove cookie as well if it is Dashboard
            var user = Session["CurrentUser"] as Person;

            if (user != null && user.Firstname.ToUpper() == "IRIS" && user.Surname.ToUpper() == "DASHBOARDS")
            {
                if (Request.Cookies["IrisDashboardUser"] != null)
                {
                    var responseCookie = Response.Cookies["IrisDashboardUser"];
                    if (responseCookie != null)
                        responseCookie.Expires = DateTime.Now.AddDays(-1);
                }
            }

            FormsAuthentication.SignOut();
            Session["CurrentUser"] = null;
            return RedirectToAction("Index", "Home");
        }


        public ActionResult Register(RegisterModel register)
        {
            var context = new ValidationContext(register, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(register, context, validationResults, true);

            if (isValid) 
            {
                try
                {
                    if (!register.Email.ToLower().Contains("@irissmart.com"))
                        register.Email = register.Email + "@irissmart.com";

                    var result = new LoginProxy().LoginChannel.RegisterUser(register.FirstName, register.Surname, register.Email, register.PhoneNumber, register.Password);

                    string msg;

                    switch (result)
                    {
                        case 0:
                            msg = "<div style='margin: 10px'>An email has been sent to you Iris mailbox.</div><div>Please click on the confirmation link in the mail to complete your registration.</div>";
                            break;
                        case 1:
                            msg = "A verification email has already been sent to your mailbox.";
                            break;
                        case 2:
                            msg = "This email address is already registered. Please recover your password if it is forgotten.";
                            break;
                        default:
                            msg = "<div style='margin: 10px'>An email has been sent to you Iris mailbox.</div><div>Please click on the confirmation link in the mail to complete your registration.</div>";
                            break;
                    }

                    return Json(new { status = result, err = msg }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    SaveError(ex);
                    return Json(new { err = "Error registering new user" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = 3, err = string.Join(", ",
                    validationResults.Select(e => e.ErrorMessage)
                    .ToArray()) }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ResetUserPassword(string email)
        {
            try
            {
                var result = new LoginProxy().LoginChannel.ResetUserPassword(email);

                var msg = "";
                if (result == 2)
                    msg = "Could not find any registered user with this email address";

                if (result == 0)
                    msg = "An email has been sent with insructions on how to reset your password";

                return Json(new { status = result, err = msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 5, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateForgottenPassword(ResetPassword resetPassword)
        {
            var context = new ValidationContext(resetPassword, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();

            var isValid = Validator.TryValidateObject(resetPassword, context, validationResults, true);

            if (isValid)
            {
                var result = new LoginProxy().LoginChannel.UpdateForgottenPassword(resetPassword.Password, resetPassword.Guid);

                var msg = result == 1 ? "Password successfully updated. Please log in with your Iris email and new password" :
                    "Could not update password";

                return Json(new { status = result, err = msg }, JsonRequestBehavior.AllowGet);
            }

            return Json(new
            {
                status = 3,
                err = string.Join(", ",
                    validationResults.Select(e => e.ErrorMessage)
                        .ToArray())
            }, JsonRequestBehavior.AllowGet);
        }

        #region Helpers

        public class HandleAntiForgeryError : ActionFilterAttribute, IExceptionFilter
        {
            public void OnException(ExceptionContext filterContext)
            {
                var exception = filterContext.Exception as HttpAntiForgeryException;
                if (exception != null)
                {
                    var routeValues = new RouteValueDictionary();
                    routeValues["controller"] = "Home";
                    routeValues["action"] = "Index";
                    filterContext.Result = new RedirectToRouteResult(routeValues);
                    filterContext.ExceptionHandled = true;
                }
            }
        }

        #endregion
    }
}