﻿using EPMS_Forensics.Helpers;
using EPMS_Forensics_Entities;
using EPMS_Forensics_Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Helper = EPMS_Forensics.Helpers.Helper;

namespace EPMS_Forensics.Controllers
{
    [UserAuthorized]
    public class ReportController : Controller
    {
        private readonly List<int> _allowedUsers = new List<int> { 228, 68, 1 };

        // GET: Report
        public ActionResult Index()
        {
            // This should only be accessible by the CGI
            var person = Session["CurrentUser"] as Person;
            if (person != null && Session["CurrentUser"] != null && !_allowedUsers.Contains(person.PersonId))
                return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult GenerateReport(string searchtype, string from, string to)
        {
            // This should only be accessible by the CGI
            var person = Session["CurrentUser"] as Person;
            if (person != null && Session["CurrentUser"] != null && !_allowedUsers.Contains(person.PersonId))
                return Json(new { status = 2, errormsg = "You do not have access to this" }, JsonRequestBehavior.AllowGet);

            try
            {
                // Get data for the interval
                var frm = string.IsNullOrEmpty(from) ? DateTime.Today : DateTime.ParseExact(from, "dd/MM/yyyy", null);
                var upto = string.IsNullOrEmpty(to) ? DateTime.Today : DateTime.ParseExact(to, "dd/MM/yyyy", null);

                var data = new InvestigatorProxy().InvestigatorChannel.GetPassportsIssued(frm, upto);

                // Calculate details
                var report = new BranchReport
                {
                    Page = 1,
                    ReportName = "Production Report",
                    ReportSubHeader = "OVERALL PASSPORTS ISSUANCE BY TYPE",
                    Interval = "( BETWEEN " + frm.ToString("dd-MMM-yyyy") + " AND " + upto.ToString("dd-MMM-yyyy") +
                               " )",
                    TimeStamp = DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm tt"),
                    SubInterval = "From " + frm.ToString("dd-MMM-yyyy") + " to " + upto.ToString("dd-MMM-yyyy"),
                    OfficialPassports = new Dictionary<string, int>(),
                    OrdinaryPassports = new Dictionary<string, int>(),
                    DiplomaticPassports = new Dictionary<string, int>()
                };


                if (searchtype == "0")
                {
                    report.HeaderName = "ALL BRANCHES (TOTAL BOOKLETS ISSUED)";
                }

                if (searchtype == "1")
                {
                    report.HeaderName = "LOCAL BRANCHES (TOTAL BOOKLETS ISSUED)";
                    report.ReportSubHeader = "LOCAL PASSPORTS ISSUANCE BY TYPE";
                }

                if (searchtype == "2")
                {
                    report.HeaderName = "FOREIGN BRANCHES (TOTAL BOOKLETS ISSUED)";
                    report.ReportSubHeader = "FOREIGN PASSPORTS ISSUANCE BY TYPE";
                }

                foreach (var rep in data)
                {
                    if (searchtype == "1")
                    {
                        // Local branches
                        if (rep.IsForeign == 1)
                            continue;
                    }

                    if (searchtype == "2")
                    {
                        // Foreign branches
                        if (rep.IsForeign == 0)
                            continue;
                    }

                    if (rep.DocType == "P")
                    {
                        report.OrdinaryPassports.Add(rep.BranchName, rep.NumberIssued);
                    }

                    if (rep.DocType == "PF")
                    {
                        report.OfficialPassports.Add(rep.BranchName, rep.NumberIssued);
                    }

                    if (rep.DocType == "PD")
                    {
                        report.DiplomaticPassports.Add(rep.BranchName, rep.NumberIssued);
                    }
                }

                report.DiplomaticPassportsTotal = report.DiplomaticPassports.Sum(x => x.Value);
                report.OrdinaryPassportsTotal = report.OrdinaryPassports.Sum(x => x.Value);
                report.OfficialPassportsTotal = report.OfficialPassports.Sum(x => x.Value);

                return Json(new { status = 1, page = Helper.JsonPartialView(this, "_BranchReport", report) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 2, errormsg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}