﻿using EPMS_Forensics.Helpers;
using EPMS_Forensics.Models.Investigator;
using EPMS_Forensics_Entities;
using EPMS_Forensics_Proxy;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Helper = EPMS_Forensics.Helpers.Helper;

namespace EPMS_Forensics.Controllers
{
    [UserAuthorized]
    public class InvestigatorController : BaseController
    {
        private static List<State> _states;
        private static List<Branch> _branches;

        #region Actions

        public ActionResult Index()
        {
            if (_branches == null)
                _branches = new InvestigatorProxy().InvestigatorChannel.GetBranches();

            if (_states == null)
                _states = new InvestigatorProxy().InvestigatorChannel.GetStates();

            // Set up branch list
            ViewBag.Branches = _branches;
            ViewBag.States = _states;

            return View();
        }

        #endregion

        #region Json

        // Search
        public ActionResult SearchByName(string surname, string firstname, string middlename, string dob, string sex, string state, string min, string max)
        {
            try
            {
                if ((string.IsNullOrEmpty(surname) && string.IsNullOrEmpty(firstname) && string.IsNullOrEmpty(middlename)) || string.IsNullOrEmpty(sex))
                    return Json(new { err = "Please enter a part of the name and gender" }, JsonRequestBehavior.AllowGet);

                var results = new SearchResults();

                // Get results
                if (Session["CurrentUser"] is Person person)
                    results.Applicants = new InvestigatorProxy().InvestigatorChannel.SearchByNameV2(surname, firstname, middlename,
                            sex, dob, state, min, max, person.PersonId);

                // filter if dob is given
                if (!string.IsNullOrEmpty(dob) && results.Applicants.Count != 0)
                {
                    var dobresult = results.Applicants.Where(x => x.Dob == DateTime.ParseExact(dob, "dd/MM/yyyy", null)).ToList();

                    if (dobresult.Count != 0)
                        results.Applicants = dobresult;
                }

                // Order by surname
                results.Applicants = results.Applicants.OrderBy(x => x.Surname).ThenBy(x => x.Middlename).ThenBy(x => x.Firstname).ToList();

                return Json(new { status = 1, num = results.Applicants.Count, page = Helper.JsonPartialView(this, "_SearchResults", results) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchByApplication(string afisquerynum, string enrolmentid, string passportnum, string idperson, string branchid)
        {
            try
            {
                if (string.IsNullOrEmpty(afisquerynum) && string.IsNullOrEmpty(enrolmentid) && string.IsNullOrEmpty(passportnum) && string.IsNullOrEmpty(idperson) && string.IsNullOrEmpty(branchid))
                    return Json(new { err = "Invalid search criteria" }, JsonRequestBehavior.AllowGet);

                // Create enrolment string
                if (!string.IsNullOrEmpty(enrolmentid) && !string.IsNullOrEmpty(branchid))
                    enrolmentid = branchid + enrolmentid.PadLeft(9, '0');

                var person = Session["CurrentUser"] as Person;
                if (string.IsNullOrEmpty(passportnum))
                    passportnum = "";

                if (person != null)
                {
                    var results = new SearchResults
                    {
                        Applicants = new InvestigatorProxy().InvestigatorChannel.SearchByApplication(afisquerynum,
                            enrolmentid, passportnum.ToUpper(), idperson, person.PersonId)
                    };

                    // Get results
                    return Json(new { status = 1, num = results.Applicants.Count, page = Helper.JsonPartialView(this, "_SearchResults", results) }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchBlacklist(string blacklistpassport)
        {
            try
            {
                if (string.IsNullOrEmpty(blacklistpassport))
                    return Json(new { err = "Invalid search criteria" }, JsonRequestBehavior.AllowGet);

                // Get results
                if (Session["CurrentUser"] is Person person)
                {
                    var results = new InvestigatorProxy().InvestigatorChannel.SearchBlacklist(blacklistpassport.ToUpper(), person.PersonId);

                    return Json(new { status = 1, num = results.Count, page = Helper.JsonPartialView(this, "_BlackList", results) }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchByBooklet(string tracebooklet)
        {
            try
            {
                if (string.IsNullOrEmpty(tracebooklet))
                    return Json(new { err = "Please enter a search variable" }, JsonRequestBehavior.AllowGet);

                if (Session["CurrentUser"] is Person person)
                {
                    var applicant = new InvestigatorProxy().InvestigatorChannel.GetTraceBooklet(tracebooklet.ToUpper(), person.PersonId);

                    if (applicant.Booklets.Count == 0)
                        return Json(new { err = "No booklets matching the entered booklet number" }, JsonRequestBehavior.AllowGet);

                    return Json(new { status = 1, page = Helper.JsonPartialView(this, "_TraceBooklet", applicant) }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchByFingerprint(string filename)
        {
            var server = ConfigurationManager.AppSettings["server"];
            var bmp = @"\\" + server + @"\Fingerprints\fingerprint.bmp";
            var wsq = @"\\" + server + @"\Fingerprints\fingerprint.wsq";
            var bmpexists = System.IO.File.Exists(bmp);
            var wsqexists = System.IO.File.Exists(wsq);

            try
            {
                if (string.IsNullOrEmpty(filename))
                {
                    if (bmpexists)
                        filename = bmp;
                    else if (wsqexists)
                        filename = wsq;
                    else
                    {
                        return Json(new { status = -1, err = "Could not find fingerprint image on server" }, JsonRequestBehavior.AllowGet);
                    }
                }

                var matches = new List<Application>();

                // Send to afis
                var url = "http://1.0.1.201/web/request/";

                using (var client = new BetterWebClient(null, false))
                {
                    client.UploadFile(url, filename);
                    var redirect = client.Location;

                    if (!string.IsNullOrEmpty(redirect))
                    {
                        // Try to check for response every second to see if afis has a result for you
                        var tries = 0;
                        while (true)
                        {
                            var response = client.DownloadString(redirect + ".json");

                            dynamic parsedObject = JsonConvert.DeserializeObject(response);
                            if (parsedObject.state == "done")
                            {
                                foreach (var entry in parsedObject.matches)
                                {
                                    var app = new Application()
                                    {
                                        FormNo = entry.form_no,
                                        QueryNo = entry.query_no,
                                        FingerprintPosition = entry.fpos,
                                        Score = entry.score
                                    };

                                    matches.Add(app);
                                }

                                break;
                            }
                            else
                            {
                                // Waiting a maximum of a minute and a half for a response from afis
                                if (tries == 40)
                                    break;

                                // wait 2 second
                                System.Threading.Thread.Sleep(2000);
                                tries++;
                            }
                        }
                    }
                }

                var results = new SearchResults();

                // log audit trail
                var desc = matches.Count == 0 ? "No matches found" : "Idperson: " + string.Join(",", matches.Select(x => x.FormNo).ToArray());
                if (Session["CurrentUser"] is Person person)
                {
                    var audit = new Audit
                    {
                        TimeStamp = DateTime.Now,
                        UserId = person.PersonId,
                        EventType = (int)AuditEnum.SearchPassportByFingerprint,
                        Description = desc
                    };

                    // Add audit trail
                    new AuditProxy().AuditChannel.AddAudit(audit);
                }

                if (matches.Count != 0)
                {
                    // Get all matches
                    foreach (var match in matches)
                    {
                        if (Session["CurrentUser"] is Person p)
                            results.Applicants.AddRange(new InvestigatorProxy().InvestigatorChannel.SearchByApplication(
                                null, match.FormNo, null, null, p.PersonId));
                    }
                }
                else
                    return Json(new { status = 0, err = "No match found for this fingerprint" }, JsonRequestBehavior.AllowGet);


                return Json(new { status = 1, num = results.Applicants.Count, page = Helper.JsonPartialView(this, "_SearchResults", results) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UploadFingeprint()
        {
            var server = ConfigurationManager.AppSettings["server"];

            var httpPostedFile = Request.Files["UploadedFile"];

            if (httpPostedFile == null)
                return Json(new {status = 0}, JsonRequestBehavior.AllowGet);

            try
            {
                // Read input stream from request
                var filename = @"\\" + server + @"\Fingerprints\fingerprint" + System.IO.Path.GetExtension(httpPostedFile.FileName);
                httpPostedFile.SaveAs(filename);
                return SearchByFingerprint(filename);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { success = 0, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        // Requests
        public ActionResult GetApplicationInfo(string id)
        {
            if (id == "Pending")
            {
                return Json(new { status = -1, err = "The applicant does not have a booklet yet" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (!(Session["CurrentUser"] is Person person))
                    return Json(new {status = 0}, JsonRequestBehavior.AllowGet);

                var applicant = new InvestigatorProxy().InvestigatorChannel.GetApplicationInfo(id, person.PersonId);

                // Order it by latest passport
                applicant = applicant.OrderByDescending(x => x.Applications[0].IssueDate).ToList();

                return Json(new { status = 1, page = Helper.JsonPartialView(this, "_ViewApplicantInfo", applicant) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetBookletInfo(string id)
        {
            if (id == "Pending")
            {
                return Json(new { status = -1, err = "The applicant does not have a booklet yet" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var person = Session["CurrentUser"] as Person;
                if (person == null)
                    return Json(new {status = 0}, JsonRequestBehavior.AllowGet);

                var applicant = new InvestigatorProxy().InvestigatorChannel.GetApplicationInfo(id, person.PersonId);

                // Order it by latest passport
                applicant = applicant.OrderBy(x => x.Applications[0].IssueDate).ToList();

                // Set Checkdigits
                foreach (var p in applicant)
                    SetCheckDigits(p);

                return Json(new { status = 1, page = Helper.JsonPartialView(this, "_ViewBookletPage", applicant) },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewAfisTransaction(string id, string formnos)
        {
            if (id.Contains("temp"))
            {
                return Json(new { status = -1, err = "No Afis details available for this record. Must be pulled from branch" }, 
                    JsonRequestBehavior.AllowGet);
            }

            try
            {
                var person = Session["CurrentUser"] as Person;
                if (person == null)
                    return Json(new {status = 0}, JsonRequestBehavior.AllowGet);

                var transactions = new InvestigatorProxy().InvestigatorChannel.GetAfisTransactions(id, formnos, person.PersonId);
                return Json(new { status = 1, page = Helper.JsonPartialView(this, "_ViewAfisTransaction", transactions) }, 
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewEnrolmentHistory(string id, string formnos)
        {
            try
            {
                var person = Session["CurrentUser"] as Person;
                if (person == null)
                    return Json(new {status = 0}, JsonRequestBehavior.AllowGet);

                var history = new InvestigatorProxy().InvestigatorChannel.GetEnrollmentHistory(formnos, person.PersonId);

                return Json(new { status = 1, page = Helper.JsonPartialView(this, "_ViewEnrolmentHistory", history) }, 
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewSummary(string id, string formnos)
        {
            if (id.Contains("temp"))
            {
                return Json(new { status = -1, err = "No Table details available for this record" }, JsonRequestBehavior.AllowGet);
            }
            try
            {
                var person = Session["CurrentUser"] as Person;
                if (person == null)
                    return Json(new {status = 0}, JsonRequestBehavior.AllowGet);

                var summary = new InvestigatorProxy().InvestigatorChannel.GetTableViewData(id, formnos, person.PersonId);
                return Json(new { status = 1, page = Helper.JsonPartialView(this, "_ViewApplicantsData", summary) }, 
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                SaveError(ex);
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Operations

        private static string CheckDigit(string str, bool preferFillerOverZero)
        {
            try
            {
                var chars = str == null ? new byte[] { } : Encoding.UTF8.GetBytes(str);
                int[] weights = { 7, 3, 1 };
                var result = 0;
                for (var i = 0; i < chars.Length; i++)
                {
                    result = (result + weights[i % 3] * DecodeMrzDigit(chars[i])) % 10;
                }
                var checkDigitString = result.ToString();
                if (checkDigitString.Length != 1)
                {
                    throw new Exception("Error in computing check digit.");
                }
                var checkDigit = (char)Encoding.UTF8.GetBytes(checkDigitString).ElementAt(0);
                if (preferFillerOverZero && checkDigit == '0')
                {
                    checkDigit = '<';
                }
                return checkDigit.ToString();
            }
            catch (FormatException nfe)
            {

                throw new Exception("Error in computing check digit. " + nfe.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Unexpected Error" + ex.Message);
            }
        }

        private static int DecodeMrzDigit(byte ch)
        {
            switch (ch)
            {
                case (byte)'<':
                case (byte)'0': return 0;
                case (byte)'1': return 1;
                case (byte)'2': return 2;
                case (byte)'3': return 3;
                case (byte)'4': return 4;
                case (byte)'5': return 5;
                case (byte)'6': return 6;
                case (byte)'7': return 7;
                case (byte)'8': return 8;
                case (byte)'9': return 9;
                case (byte)'a': case (byte)'A': return 10;
                case (byte)'b': case (byte)'B': return 11;
                case (byte)'c': case (byte)'C': return 12;
                case (byte)'d': case (byte)'D': return 13;
                case (byte)'e': case (byte)'E': return 14;
                case (byte)'f': case (byte)'F': return 15;
                case (byte)'g': case (byte)'G': return 16;
                case (byte)'h': case (byte)'H': return 17;
                case (byte)'i': case (byte)'I': return 18;
                case (byte)'j': case (byte)'J': return 19;
                case (byte)'k': case (byte)'K': return 20;
                case (byte)'l': case (byte)'L': return 21;
                case (byte)'m': case (byte)'M': return 22;
                case (byte)'n': case (byte)'N': return 23;
                case (byte)'o': case (byte)'O': return 24;
                case (byte)'p': case (byte)'P': return 25;
                case (byte)'q': case (byte)'Q': return 26;
                case (byte)'r': case (byte)'R': return 27;
                case (byte)'s': case (byte)'S': return 28;
                case (byte)'t': case (byte)'T': return 29;
                case (byte)'u': case (byte)'U': return 30;
                case (byte)'v': case (byte)'V': return 31;
                case (byte)'w': case (byte)'W': return 32;
                case (byte)'x': case (byte)'X': return 33;
                case (byte)'y': case (byte)'Y': return 34;
                case (byte)'z': case (byte)'Z': return 35;
                default:
                    throw new FormatException("Could not decode MRZ character "
                                              + ch + "('" + ch + ")'");
            }
        }

        private static void SetCheckDigits(Person person)
        {
            var app = person.Applications[0];

            if (app == null)
                return;

            var dob = person.Dob.ToString("yyMMdd");
            var doe = app.ExpiryDate.ToString("yyMMdd");
            app.DocumentNoCheckDigit = CheckDigit(app.DocumentNo, false);
            app.DobCheckDigit = CheckDigit(dob, false);
            app.ExpiryDateCheckDigit = CheckDigit(doe, false);

            var val = app.DocumentNo + app.DocumentNoCheckDigit + dob + app.DobCheckDigit
                         + doe + app.ExpiryDateCheckDigit;
            app.CompositeCheckDigit = "0" + CheckDigit(val, false);

            app.Mrz1 = app.ApplicationType.ToUpper() + "<NGR" + person.Surname.ToUpper() + "<<" +
                       person.Firstname.ToUpper()
                       + "<" + person.Middlename.ToUpper();

            // add filling chars
            while (app.Mrz1.Length < 44)
                app.Mrz1 += "<";

            app.Mrz2 = app.DocumentNo + app.DocumentNoCheckDigit + "NGA" + person.Dob.ToString("yyMMdd") +
                       app.DobCheckDigit + person.Gender.ToUpper() + app.ExpiryDate.ToString("yyMMdd") + app.ExpiryDateCheckDigit;

            // add filling chars
            while (app.Mrz2.Length < 42)
                app.Mrz2 += "<";

            app.Mrz2 += app.CompositeCheckDigit;
        }

        #endregion
    }
}