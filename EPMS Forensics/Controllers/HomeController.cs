﻿using EPMS_Forensics_Entities;
using EPMS_Forensics_Proxy;
using System;
using System.Web.Mvc;

namespace EPMS_Forensics.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string guid, string reset)
        {
            try
            {
                if (!string.IsNullOrEmpty(guid))
                {
                    var result = new LoginProxy().LoginChannel.ActivateUser(guid);

                    if (result == 1)
                        ViewBag.Msg = "Account successfully activated. Please enter your username and password to log in";
                }

                if (!string.IsNullOrEmpty(reset))
                {
                    var result = new LoginProxy().LoginChannel.VerifyPasswordChange(reset);

                    if (result == 0)
                    {
                        ViewBag.ShowUpdatePassword = 1;
                        ViewBag.Guid = reset;
                    }

                    if (result == 1)
                        ViewBag.Msg = "Invalid request";

                    if (result == 2)
                        ViewBag.Msg = "<div style='margin-bottom: 10px'>Your request to change password has expired. Please do another request.</div>";
                }
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
            }

            return View();
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}