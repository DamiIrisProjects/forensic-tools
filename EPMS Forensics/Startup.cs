﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EPMS_Forensics.Startup))]
namespace EPMS_Forensics
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
