﻿using EPMS_Forensics_Entities;
using EPMS_Forensics_Proxy;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace EPMS_Forensics
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];

            // Custom case with dashboard which I want to keep forever logged in
            if (authCookie == null)
            {
                authCookie = Request.Cookies["IrisDashboardUser"];
            }

            if (authCookie != null)
            {
                //Extract the forms authentication cookie
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                // If caching roles in userData field then extract
                string[] roles = { }; //authTicket.UserData.Split(new char[] { '|' });
               
                // Create the IIdentity instance
                IIdentity id = new FormsIdentity(authTicket);

                // Create the IPrinciple instance
                IPrincipal principal = new GenericPrincipal(id, roles);

                // Set the context user 
                Context.User = principal;
            }
        }

        private void Application_Error(object sender, EventArgs e)
        {
            var exc = Server.GetLastError();
            new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(exc));
        }
    }
}
