﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EPMS_Forensics.Models;
using System.Web.Routing;
using System.Web.Security;
using EPMS_Forensics_Proxy;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using EPMS_Forensics_Entities;

namespace EPMS_Forensics.Controllers
{
    public class AccountController : Controller
    {
        //[HandleAntiForgeryError]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                Person user = new Person();

                try
                {
                    LoginProxy proxy = new LoginProxy();
                    user = proxy.LoginChannel.LoginUser(model.Email, model.Password);

                    if (user == null)
                    {
                        ModelState.AddModelError("", "Invalid Username or Password");
                        return View();
                    }
                    else
                    {
                        Session["CurrentUser"] = user;

                        if (model.Email == "irisdash@irissmart.com")
                        {
                            bool persist = true;
                            var cookie = FormsAuthentication.GetAuthCookie(user.FullName, persist);
                            cookie.Name = "IrisDashboards";
                            cookie.Expires = DateTime.Now.AddMonths(12);
                            var ticket = FormsAuthentication.Decrypt(cookie.Value);
                            var userData = new UserData() { Firstname = user.Firstname, Surname = user.Surname, Id = user.PersonId };
                            var newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name,
                                 ticket.IssueDate, ticket.Expiration, ticket.IsPersistent, new JavaScriptSerializer().Serialize(userData));
                            cookie.Value = FormsAuthentication.Encrypt(newTicket);
                            Response.Cookies.Add(cookie);
                        }
                        else
                            FormsAuthentication.SetAuthCookie(user.FullName, false);

                        return RedirectToAction("Index", "Home");
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                    }
                    catch (Exception)
                    { throw; }

                    return RedirectToAction("Error");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home", new { id = "Retry" });
            }
        }

        [Authorize]
        [HttpPost]
        [HandleAntiForgeryError]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut(string returnUrl)
        {
            // Remove cookie as well if it is Dashboard
            Person user = Session["CurrentUser"] as Person;

            if (user.Firstname.ToUpper() == "IRIS" && user.Surname.ToUpper() == "DASHBOARDS")
            {
                if (Request.Cookies["IrisDashboards"] != null)
                {
                    Response.Cookies["IrisDashboards"].Expires = DateTime.Now.AddDays(-1);
                }
            }

            FormsAuthentication.SignOut();
            Session["CurrentUser"] = null;
            return RedirectToAction("Index", "Home");
        }


        public ActionResult Register(RegisterModel Register)
        {
            var context = new ValidationContext(Register, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();

            bool isValid = Validator.TryValidateObject(Register, context, validationResults, true);

            if (isValid) 
            {
                try
                {
                    if (!Register.Email.ToLower().Contains("@irissmart.com"))
                        Register.Email = Register.Email + "@irissmart.com";

                    int result = new LoginProxy().LoginChannel.RegisterUser(Register.FirstName, Register.Surname, Register.Email, Register.PhoneNumber, Register.Password);

                    string msg = string.Empty;

                    if (result == 0)
                        msg = "<div style='margin: 10px'>An email has been sent to you Iris mailbox.</div><div>Please click on the confirmation link in the mail to complete your registration.</div>";

                    else if (result == 1)
                        msg = "A verification email has already been sent to your mailbox.";

                    else if (result == 2)
                        msg = "This email address is already registered. Please recover your password if it is forgotten.";

                    else
                        msg = "<div style='margin: 10px'>An email has been sent to you Iris mailbox.</div><div>Please click on the confirmation link in the mail to complete your registration.</div>";

                    return Json(new { status = result, err = msg }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    try
                    {
                        new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                    }
                    catch (Exception)
                    { throw; }

                    return Json(new { err = "Error registering new user" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = 3, err = string.Join(", ",
                    validationResults.Select(E => E.ErrorMessage)
                    .ToArray()) }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ResetUserPassword(string email)
        {
            try
            {
                int result = new LoginProxy().LoginChannel.ResetUserPassword(email);

                string msg = "";
                if (result == 2)
                    msg = "Could not find any registered user with this email address";

                if (result == 0)
                    msg = "An email has been sent with insructions on how to reset your password";

                return Json(new { status = result, err = msg }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 5, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateForgottenPassword(ResetPassword ResetPassword)
        {
            var context = new ValidationContext(ResetPassword, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();

            bool isValid = Validator.TryValidateObject(ResetPassword, context, validationResults, true);

            if (isValid)
            {
                try
                {
                    int result = new LoginProxy().LoginChannel.UpdateForgottenPassword(ResetPassword.Password, ResetPassword.Guid);

                    string msg = "";

                    if (result == 1)
                        msg = "Password successfully updated. Please log in with your Iris email and new password";
                    else
                        msg = "Could not update password";

                    return Json(new { status = result, err = msg }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            else
            {
                return Json(new
                {
                    status = 3,
                    err = string.Join(", ",
                        validationResults.Select(E => E.ErrorMessage)
                        .ToArray())
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Helpers

        public class HandleAntiForgeryError : ActionFilterAttribute, IExceptionFilter
        {
            public void OnException(ExceptionContext filterContext)
            {
                var exception = filterContext.Exception as HttpAntiForgeryException;
                if (exception != null)
                {
                    var routeValues = new RouteValueDictionary();
                    routeValues["controller"] = "Home";
                    routeValues["action"] = "Index";
                    filterContext.Result = new RedirectToRouteResult(routeValues);
                    filterContext.ExceptionHandled = true;
                }
            }
        }

        #endregion
    }
}