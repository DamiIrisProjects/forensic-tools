﻿using EPMS_Forensics.Helpers;
using EPMS_Forensics.Models.Investigator;
using EPMS_Forensics_Entities;
using EPMS_Forensics_Entities.Summary;
using EPMS_Forensics_Proxy;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;

namespace EPMS_Forensics.Controllers
{
    [UserAuthorized]
    public class InvestigatorController : Controller
    {
        private static List<State> States;
        private static List<Branch> Branches;

        #region Actions

        public ActionResult Index()
        {
            if (Branches == null)
                Branches = new InvestigatorProxy().InvestigatorChannel.GetBranches();

            if (States == null)
                States = new InvestigatorProxy().InvestigatorChannel.GetStates();

            // Set up branch list
            ViewBag.Branches = Branches;
            ViewBag.States = States;

            return View();
        }

        #endregion

        #region Json

        // Search
        public ActionResult SearchByName(string surname, string firstname, string middlename, string dob, string sex, string state, string min, string max)
        {
            try
            {
                if ((string.IsNullOrEmpty(surname) && string.IsNullOrEmpty(firstname) && string.IsNullOrEmpty(middlename)) || string.IsNullOrEmpty(sex))
                    return Json(new { err = "Please enter a part of the name and gender" }, JsonRequestBehavior.AllowGet);

                //int count = 0;

                //if (!string.IsNullOrEmpty(surname)) count++;
                //if (!string.IsNullOrEmpty(firstname)) count++;
                //if (!string.IsNullOrEmpty(middlename)) count++;
                //if (!string.IsNullOrEmpty(dob)) count++;
                //if (count < 2)
                //    return Json(new { err = "Please enter at least 2 variables" }, JsonRequestBehavior.AllowGet);

                SearchResults results =  new SearchResults();

                // Get results
                results.Applicants = new InvestigatorProxy().InvestigatorChannel.SearchByName(surname, firstname, middlename, dob, (Session["CurrentUser"] as Person).PersonId);
                //results.Applicants = new InvestigatorProxy().InvestigatorChannel.SearchByNameV2(surname, firstname, middlename, sex, dob, state, min, max, (Session["CurrentUser"] as Person).PersonId);

                return Json(new { status = 1, num = results.Applicants.Count, page = iHelper.JsonPartialView(this, "_SearchResults", results) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchByApplication(string afisquerynum, string enrolmentid, string passportnum, string idperson, string branchid)
        {
            try
            {
                if (string.IsNullOrEmpty(afisquerynum) && string.IsNullOrEmpty(enrolmentid) && string.IsNullOrEmpty(passportnum) && string.IsNullOrEmpty(idperson) && string.IsNullOrEmpty(branchid))
                    return Json(new { err = "Invalid search criteria" }, JsonRequestBehavior.AllowGet);

                // Create enrolment string
                if (!string.IsNullOrEmpty(enrolmentid) && !string.IsNullOrEmpty(branchid))
                    enrolmentid = branchid + enrolmentid.PadLeft(9, '0');

                SearchResults results = new SearchResults();

                // Get results
                results.Applicants = new InvestigatorProxy().InvestigatorChannel.SearchByApplication(afisquerynum, enrolmentid, passportnum.ToUpper(), idperson, (Session["CurrentUser"] as Person).PersonId);

                return Json(new { status = 1, num = results.Applicants.Count, page = iHelper.JsonPartialView(this, "_SearchResults", results) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchBlacklist(string blacklistpassport)
        {
            try
            {
                if (string.IsNullOrEmpty(blacklistpassport))
                    return Json(new { err = "Invalid search criteria" }, JsonRequestBehavior.AllowGet);

                // Get results
                List<Person> results = new InvestigatorProxy().InvestigatorChannel.SearchBlacklist(blacklistpassport.ToUpper(), (Session["CurrentUser"] as Person).PersonId);

                return Json(new { status = 1, num = results.Count, page = iHelper.JsonPartialView(this, "_BlackList", results) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchByBooklet(string tracebooklet)
        {
            try
            {
                if (string.IsNullOrEmpty(tracebooklet))
                    return Json(new { err = "Please enter a search variable" }, JsonRequestBehavior.AllowGet);

                Application applicant = new InvestigatorProxy().InvestigatorChannel.GetTraceBooklet(tracebooklet.ToUpper(), (Session["CurrentUser"] as Person).PersonId);

                if (applicant.Booklets.Count == 0)
                    return Json(new { err = "No booklets matching the entered booklet number" }, JsonRequestBehavior.AllowGet);

                return Json(new { status = 1, page = iHelper.JsonPartialView(this, "_TraceBooklet", applicant) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SearchByFingerprint(string filename)
        {
            string server = ConfigurationManager.AppSettings["server"].ToString();
            string bmp = @"\\" + server + @"\Fingerprints\fingerprint.bmp";
            string wsq = @"\\" + server + @"\Fingerprints\fingerprint.wsq";
            bool bmpexists = System.IO.File.Exists(bmp);
            bool wsqexists = System.IO.File.Exists(wsq);

            try
            {
                if (string.IsNullOrEmpty(filename))
                {
                    if (bmpexists)
                        filename = bmp;
                    else if (wsqexists)
                        filename = wsq;
                    else
                    {
                        return Json(new { status = -1, err = "Could not find fingerprint image on server" }, JsonRequestBehavior.AllowGet);
                    }
                }

                List<Application> matches = new List<Application>();

                // Send to afis
                string url = "http://1.0.1.201/web/request/";

                using (BetterWebClient client = new BetterWebClient(null, false))
                {
                    byte[] responsebytes = client.UploadFile(url, filename);
                    string redirect = client.Location;

                    if (!string.IsNullOrEmpty(redirect))
                    {
                        // Try to check for response every second to see if afis has a result for you
                        int tries = 0;
                        while (true)
                        {
                            string response = client.DownloadString(redirect + ".json");

                            dynamic parsedObject = JsonConvert.DeserializeObject(response);
                            if (parsedObject.state == "done")
                            {
                                foreach (dynamic entry in parsedObject.matches)
                                {
                                    Application app = new Application()
                                    {
                                        FormNo = entry.form_no,
                                        QueryNo = entry.query_no,
                                        FingerprintPosition = entry.fpos,
                                        Score = entry.score
                                    };

                                    matches.Add(app);
                                }

                                break;
                            }
                            else
                            {
                                // Waiting a maximum of a minute and a half for a response from afis
                                if (tries == 40)
                                    break;

                                // wait 2 second
                                System.Threading.Thread.Sleep(2000);
                                tries++;
                            }
                        }
                    }
                }

                SearchResults results = new SearchResults();

                // log audit trail
                string desc = matches.Count == 0 ? "No matches found" : "Idperson: " + string.Join(",", matches.Select(x => x.FormNo).ToArray());
                Audit audit = new Audit()
                {
                    TimeStamp = DateTime.Now,
                    UserId = (Session["CurrentUser"] as Person).PersonId,
                    EventType = (int)AuditEnum.SearchPassportByFingerprint,
                    Description =  desc
                };

                // Add audit trail
                new AuditProxy().AuditChannel.AddAudit(audit);

                if (matches.Count != 0)
                {
                    // Get all matches
                    foreach (Application match in matches)
                    {
                        results.Applicants.AddRange(new InvestigatorProxy().InvestigatorChannel.SearchByApplication(null, match.FormNo, null, null, (Session["CurrentUser"] as Person).PersonId));
                    }
                }
                else
                    return Json(new { status = 0, err = "No match found for this fingerprint" }, JsonRequestBehavior.AllowGet);


                return Json(new { status = 1, num = results.Applicants.Count, page = iHelper.JsonPartialView(this, "_SearchResults", results) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UploadFingeprint()
        {
            string server = ConfigurationManager.AppSettings["server"].ToString();

            var httpPostedFile = Request.Files["UploadedFile"];

            if (httpPostedFile != null)
            {
                try
                {
                    // Read input stream from request
                    string filename = @"\\" + server + @"\Fingerprints\fingerprint" + System.IO.Path.GetExtension(httpPostedFile.FileName);
                    httpPostedFile.SaveAs(filename);
                    return SearchByFingerprint(filename);
                }
                catch (Exception ex)
                {
                    new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                    return Json(new { success = 0, err = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }

        // Requests
        public ActionResult GetApplicationInfo(string id)
        {
            if (id == "Pending")
            {
                return Json(new { status = -1, err = "The applicant does not have a booklet yet" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                List<Person> applicant = new InvestigatorProxy().InvestigatorChannel.GetApplicationInfo(id, (Session["CurrentUser"] as Person).PersonId);

                // Order it by latest passport
                applicant = applicant.OrderByDescending(x => x.Applications[0].IssueDate).ToList();

                return Json(new { status = 1, page = iHelper.JsonPartialView(this, "_ViewApplicantInfo", applicant) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetBookletInfo(string id)
        {
            if (id == "Pending")
            {
                return Json(new { status = -1, err = "The applicant does not have a booklet yet" }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                List<Person> applicant = new InvestigatorProxy().InvestigatorChannel.GetApplicationInfo(id, (Session["CurrentUser"] as Person).PersonId);

                // Order it by latest passport
                applicant = applicant.OrderBy(x => x.Applications[0].IssueDate).ToList();

                return Json(new { status = 1, page = iHelper.JsonPartialView(this, "_ViewBookletPage", applicant) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewAfisTransaction(string id, string formnos)
        {
            if (id.Contains("temp"))
            {
                return Json(new { status = -1, err = "No Afis details available for this record. Must be pulled from branch" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    List<Person> transactions = new InvestigatorProxy().InvestigatorChannel.GetAfisTransactions(id, formnos, (Session["CurrentUser"] as Person).PersonId);
                    return Json(new { status = 1, page = iHelper.JsonPartialView(this, "_ViewAfisTransaction", transactions) }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                    return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult ViewEnrolmentHistory(string id, string formnos)
        {
            try
            {
                List<Application> history = new InvestigatorProxy().InvestigatorChannel.GetEnrollmentHistory(formnos, (Session["CurrentUser"] as Person).PersonId);
                return Json(new { status = 1, page = iHelper.JsonPartialView(this, "_ViewEnrolmentHistory", history) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewSummary(string id, string formnos)
        {
            if (id.Contains("temp"))
            {
                return Json(new { status = -1, err = "No Table details available for this record" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    Summary summary = new InvestigatorProxy().InvestigatorChannel.GetTableViewData(id, formnos, (Session["CurrentUser"] as Person).PersonId);
                    return Json(new { status = 1, page = iHelper.JsonPartialView(this, "_ViewApplicantsData", summary) }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    new ErrorProxy().ErrorChannel.SaveError(Helper.CreateError(ex));
                    return Json(new { status = -1, err = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }        

        #endregion
    }
}