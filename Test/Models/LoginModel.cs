﻿using EPMS_Forensics.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace EPMS_Forensics.Models
{
    public class LoginViewModel
    {
        public LoginViewModel()
        {
            Register = new RegisterModel();
            ResetPassword = new ResetPassword();
        }

        [Required(ErrorMessage = "Please enter Email")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public RegisterModel Register { get; set; }

        public ResetPassword ResetPassword { get; set; }
    }

}