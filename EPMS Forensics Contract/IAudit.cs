﻿using EPMS_Forensics_Entities;
using System.ServiceModel;

namespace EPMS_Forensics_Contract
{
    [ServiceContract]
    public interface IAudit
    {
        [OperationContract]
        void AddAudit(Audit audit);
    }
}
