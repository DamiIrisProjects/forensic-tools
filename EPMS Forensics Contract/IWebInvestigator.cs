﻿using EPMS_Forensics_Entities;
using System.Collections.Generic;
using System.ServiceModel;

namespace EPMS_Forensics_Contract
{
    [ServiceContract]
    public interface IWebInvestigator
    {
        [OperationContract]
        List<Branch> GetBranches();

        [OperationContract]
        LocPassport GetPassportDetails(string formno, string bio);

        [OperationContract]
        LocPassport GetPassportDetailsDocNo(string docno, string bio);
    }
}
