﻿using EPMS_Forensics_Entities;
using System.ServiceModel;

namespace EPMS_Forensics_Contract
{
    [ServiceContract]
    public interface ILogin
    {
        [OperationContract]
        Person LoginUser(string email, string password);

        [OperationContract]
        int RegisterUser(string firstname, string surname, string email, string phonenumber, string password);

        [OperationContract]
        int ResetUserPassword(string email);

        [OperationContract]
        int ActivateUser(string password);

        [OperationContract]
        int UpdateForgottenPassword(string password, string guid);

        [OperationContract]
        int VerifyPasswordChange(string guid);
    }
}
