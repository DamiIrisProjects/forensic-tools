﻿using EPMS_Forensics_Entities;
using EPMS_Forensics_Entities.Summary;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace EPMS_Forensics_Contract
{
    [ServiceContract]
    public interface IInvestigator
    {
        [OperationContract]
        List<Branch> GetBranches();

        [OperationContract]
        List<State> GetStates();

        [OperationContract]
        List<Person> SearchByName(string surname, string firstname, string middlename, string dob, int userid);

        [OperationContract]
        List<Person> SearchByNameV2(string surname, string firstname, string middlename, string sex, string dob, string state, string min, string max, int userid);

        [OperationContract]
        List<Person> SearchByApplication(string afisquerynum, string enrolmentid, string passportnum, string idperson, int userid);

        [OperationContract]
        List<Person> SearchBlacklist(string passportnum, int userid);

        [OperationContract]
        List<Person> GetApplicationInfo(string id, int userid);

        [OperationContract]
        List<Application> GetEnrollmentHistory(string id, int userid);

        [OperationContract]
        List<Person> GetAfisTransactions(string id, string formnos, int userid);

        [OperationContract]
        Summary GetTableViewData(string id, string formnos, int userid);

        [OperationContract]
        Application GetTraceBooklet(string id, int userid);

        [OperationContract]
        int SaveFingerprint(byte[] fingerprint);

        [OperationContract]
        int ClearFingerprint();

        [OperationContract]
        List<PassportReport> GetPassportsIssued(DateTime start, DateTime end);
    }
}
