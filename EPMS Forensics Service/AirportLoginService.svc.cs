﻿using EPMS_Forensics_Contract;
using EPMS_Forensics_Data;
using EPMS_Forensics_Entities;
using System;

namespace EPMS_Forensics_Service
{
    public class AirportLoginService : IAirportLogin
    {
        public Person LoginUser(string username, string password)
        {
            var airportLoginData = new AirportLoginData();
            var auditdata = new AuditData();

            // Get user details
            var user = airportLoginData.GetUserDetails(username);

            if (user != null)
            {
                var audit = new Audit()
                {
                    TimeStamp = DateTime.Now,
                    UserId = user.PersonId
                };

                // Verify password
                var sh = SaltedHash.Create(user.Salt, user.Hash);

                if (sh.Verify(password))
                {
                    //Login success audit
                    audit.EventType = (int)AuditEnum.LoginSuccess;
                    auditdata.AddAudit(audit);
                    return user;
                }
                else
                {
                    //Login failed audit
                    audit.EventType = (int)AuditEnum.LoginFailed;
                    auditdata.AddAudit(audit);
                }
            }

            return null;
        }

        public int RegisterUser(string firstname, string surname, string email, string phonenumber, string password)
        {
            var data = new AirportLoginData();
            var result = data.VerifyUsernameDoesNotExists(firstname, surname, email, phonenumber);

            if (result == 0)
            {
                return data.RegisterUser(firstname, surname, phonenumber, email, password);
            }

            return result;
        }

        public int ResetUserPassword(string username)
        {
            return new AirportLoginData().ResetUserPassword(username);
        }

        public int ActivateUser(string email)
        {
            return new AirportLoginData().ActivateUser(email);
        }

        public int VerifyPasswordChange(string email)
        {
            return new AirportLoginData().VerifyPasswordChange(email);
        }

        public int UpdateForgottenPassword(string password, string guid)
        {
            return new AirportLoginData().UpdateForgottenPassword(password, guid);
        }
    }
}
