﻿using EPMS_Forensics_Contract;
using EPMS_Forensics_Data;
using EPMS_Forensics_Entities;
using System.Collections.Generic;
using System.ServiceModel.Web;

namespace EPMS_Forensics_Service
{
    public class WebInvestigatorService : IWebInvestigator
    {
        // Lists
        [WebGet(UriTemplate = "/getbranches",
            ResponseFormat = WebMessageFormat.Json)]
        public List<Branch> GetBranches()
        {
            return new InvestigatorData().GetBranches();
        }

        // Tables
        [WebGet(UriTemplate = "/getpassportdetails/{formno}/{bio}",
            ResponseFormat = WebMessageFormat.Json)]
        public LocPassport GetPassportDetails(string formno, string bio)
        {
            return new InvestigatorData().GetPassportDetails(formno, bool.Parse(bio));
        }

        [WebGet(UriTemplate = "/getpassportdetailsdocno/{docno}/{bio}",
            ResponseFormat = WebMessageFormat.Json)]
        public LocPassport GetPassportDetailsDocNo(string docno, string bio)
        {
            return new InvestigatorData().GetPassportDetailsDocNo(docno, bool.Parse(bio));
        }
    }
}
