﻿using EPMS_Forensics_Contract;
using EPMS_Forensics_Data;
using EPMS_Forensics_Entities;
using System;

namespace EPMS_Forensics_Service
{
    public class LoginService : ILogin
    {
        public Person LoginUser(string username, string password)
        {
            var logindata = new LoginData();
            var auditdata = new AuditData();

            // Get user details
            var user = logindata.GetUserDetails(username);

            if (user != null)
            {
                var audit = new Audit()
                {
                    TimeStamp = DateTime.Now,
                    UserId = user.PersonId
                };

                // Verify password
                var sh = SaltedHash.Create(user.Salt, user.Hash);

                if (sh.Verify(password))
                {
                    //Login success audit
                    audit.EventType = (int)AuditEnum.LoginSuccess;
                    auditdata.AddAudit(audit);
                    return user;
                }

                //Login failed audit
                audit.EventType = (int)AuditEnum.LoginFailed;
                auditdata.AddAudit(audit);
            }

            return null;
        }

        public int RegisterUser(string firstname, string surname, string email, string phonenumber, string password)
        {
            var data = new LoginData();
            var result = data.VerifyUsernameDoesNotExists(email);

            if (result == 0)
            {
                return data.RegisterUser(firstname, surname, phonenumber, email, password);
            }

            return result;
        }

        public int ResetUserPassword(string username)
        {
            return new LoginData().ResetUserPassword(username);
        }

        public int ActivateUser(string email)
        {
            return new LoginData().ActivateUser(email);
        }

        public int VerifyPasswordChange(string email)
        {
            return new LoginData().VerifyPasswordChange(email);
        }

        public int UpdateForgottenPassword(string password, string guid)
        {
            return new LoginData().UpdateForgottenPassword(password, guid);
        }
    }
}
