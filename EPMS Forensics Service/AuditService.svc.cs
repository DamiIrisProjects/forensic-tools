﻿using EPMS_Forensics_Contract;
using EPMS_Forensics_Data;
using EPMS_Forensics_Entities;

namespace EPMS_Forensics_Service
{
    public class AuditService : IAudit
    {
        public void AddAudit(Audit audit)
        {
            new AuditData().AddAudit(audit);
        }
    }
}
