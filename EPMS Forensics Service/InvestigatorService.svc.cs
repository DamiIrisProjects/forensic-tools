﻿using EPMS_Forensics_Contract;
using EPMS_Forensics_Data;
using EPMS_Forensics_Entities;
using EPMS_Forensics_Entities.Summary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;

namespace EPMS_Forensics_Service
{
    public class InvestigatorService : IInvestigator
    {
        // Lists
        public List<Branch> GetBranches()
        {
            return new InvestigatorData().GetBranches();
        }

        public List<State> GetStates()
        {
            return new InvestigatorData().GetStates();
        }

        // Searches
        public List<Person> SearchByName(string surname, string firstname, string middlename, string dob, int userid)
        {
            // Set audit
            var audit = new Audit()
            {
                TimeStamp = DateTime.Now,
                UserId = userid,
                Description = string.Empty
            };

            // Create search string and parameters
            var searchstring = string.Empty;
            var startstring = "WHERE ep.idperson IN (SELECT m.idperson FROM DocHolderMainProfile m INNER JOIN Docholdercustomprofile c ON m.idperson = c.idperson WHERE ";
            var param = new List<OracleParameter>();

            if (!string.IsNullOrEmpty(surname))
            {
                param.Add(new OracleParameter("surname", surname.ToUpper()));
                audit.EventType = (int)AuditEnum.SearchPassportByApplicant;
                audit.Description = "Surname: " + surname;

                searchstring = startstring + "m.surname = :surname";
            }

            if (!string.IsNullOrEmpty(firstname))
            {
                param.Add(new OracleParameter("firstname", firstname.ToUpper()));
                audit.EventType = (int)AuditEnum.SearchPassportByApplicant;
                audit.Description += audit.Description.Trim() == "" ? ("Firstname: " + firstname) : (", Firstname: " + firstname);

                if (string.IsNullOrEmpty(searchstring))
                    searchstring = startstring + "m.firstname = :firstname";
                else
                    searchstring += " and m.firstname = :firstname";
            }

            if (!string.IsNullOrEmpty(middlename))
            {
                param.Add(new OracleParameter("othername1", middlename.ToUpper()));
                audit.EventType = (int)AuditEnum.SearchPassportByApplicant;
                audit.Description += audit.Description.Trim() == "" ? ("Middlename: " + middlename) : (", Middlename: " + middlename);

                if (string.IsNullOrEmpty(searchstring))
                    searchstring = startstring + @"c.othername1 = :othername1";
                else
                    searchstring += " and c.othername1 = :othername1";
            }

            if (!string.IsNullOrEmpty(dob))
            {
                param.Add(new OracleParameter("birthdate", DateTime.ParseExact(dob, "dd/MM/yyyy", null).ToString("dd-MMM-yyyy")));
                audit.EventType = (int)AuditEnum.SearchPassportByApplicant;
                audit.Description += audit.Description.Trim() == "" ? ("DOB: " + dob) : (", DOB: " + dob);

                if (string.IsNullOrEmpty(searchstring))
                    searchstring = startstring + "m.birthdate = TO_DATE(:birthdate,'dd-MON-yyyy')";
                else
                    searchstring += " and m.birthdate = TO_DATE(:birthdate,'dd-MON-yyyy')";
            }

            // Add audit trail
            new AuditData().AddAudit(audit);

            return new InvestigatorData().SearchForPassportHolder(searchstring + ") ", param, null, null);
        }

        public List<Person> SearchByNameV2(string surname, string firstname, string middlename, string sex, string dob, string state, string min, string max, int userid)
        {
            // Create search string and parameters
            var name = string.Empty;
            var param = new List<OracleParameter>();

            if (!string.IsNullOrEmpty(surname))
            {
                name += surname;
            }

            if (!string.IsNullOrEmpty(firstname))
            {
                if (string.IsNullOrEmpty(name))
                    name += firstname;
                else
                    name += " " + firstname;
            }

            if (!string.IsNullOrEmpty(middlename))
            {
                if (string.IsNullOrEmpty(name))
                    name += firstname;
                else
                    name += " " + middlename;
            }

            // Do preliminary search
            param.Add(new OracleParameter("p_namestr", name));
            param.Add(new OracleParameter("p_sex", sex));

            if (!string.IsNullOrEmpty(min))
                param.Add(new OracleParameter("p_age_min", min));

            if (!string.IsNullOrEmpty(max))
                param.Add(new OracleParameter("p_age_max", max));

            if (!string.IsNullOrEmpty(state))
                param.Add(new OracleParameter("p_originstate", state));

            if (!string.IsNullOrEmpty(dob))
            {
                var p = new OracleParameter("p_dob", OracleType.DateTime)
                {
                    Value = DateTime.ParseExact(dob, "dd/MM/yyyy", null).ToString("dd-MMM-yyyy")
                };
                param.Add(p);
            }

            var result = new List<Person>();

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunStoredProcedure(param.ToArray(), dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    foreach (DataRow row in dset.Tables[0].Rows)
                        // Search by application instead
                        result.AddRange(SearchByApplication(null, null, null, row["idperson"].ToString(), userid));
                }
            }

            return result;
        }

        public List<Person> SearchByApplication(string afisquerynum, string enrolmentid, string passportnum, string idperson, int userid)
        {
            // Set audit
            var audit = new Audit()
            {
                TimeStamp = DateTime.Now,
                UserId = userid
            };

            var searchstring = string.Empty;
            var param = new List<OracleParameter>();

            if (!string.IsNullOrEmpty(afisquerynum))
            {
                param.Add(new OracleParameter("queryno", afisquerynum));
                searchstring = @"WHERE ep.formno IN (SELECT formno from afisquery where queryno = :queryno)";

                audit.EventType = (int)AuditEnum.SearchPassportByApplicant;
                audit.Description = "Queryno: " + afisquerynum;
            }

            else if (!string.IsNullOrEmpty(enrolmentid))
            {
                param.Add(new OracleParameter("formno", enrolmentid));
                searchstring = @"WHERE ep.idperson IN (SELECT idperson FROM enrolprofile WHERE formno = :formno)";

                audit.EventType = (int)AuditEnum.SearchPassportByApplicant;
                audit.Description = "Enrolmentno: " + enrolmentid;
            }

            else if (!string.IsNullOrEmpty(passportnum))
            {
                param.Add(new OracleParameter("docno", passportnum));
                searchstring = @"WHERE ep.formno IN (SELECT formno FROM docprofile WHERE docno = :docno)";

                audit.EventType = (int)AuditEnum.SearchPassportByApplicant;
                audit.Description = "DocNo: " + passportnum;
            }

            else if (!string.IsNullOrEmpty(idperson))
            {
                param.Add(new OracleParameter("idperson", idperson));
                searchstring = @"WHERE ep.formno IN (SELECT formno FROM EnrolProfile WHERE idperson = :idperson)";

                audit.EventType = (int)AuditEnum.SearchPassportByApplicant;
                audit.Description = "Idperson: " + idperson;
            }

            // Add audit trail
            new AuditData().AddAudit(audit);

            return new InvestigatorData().SearchForPassportHolder(searchstring, param, null, null);
        }

        public List<Person> SearchBlacklist(string passportnum, int userid)
        {
            // Set audit
            var audit = new Audit()
            {
                TimeStamp = DateTime.Now,
                UserId = userid,
                EventType = (int)AuditEnum.SearchBlacklist,
                Description = "DocNo: " + passportnum
            };

            new AuditData().AddAudit(audit);

            return new InvestigatorData().SearchBlacklist(passportnum);
        }

        // Requests
        public List<Person> GetApplicationInfo(string id, int userid)
        {
            // Set audit
            var audit = new Audit()
            {
                TimeStamp = DateTime.Now,
                UserId = userid,
                EventType = (int)AuditEnum.GetApplicationInformation,
                Description = "Idperson: " + id
            };

            new AuditData().AddAudit(audit);

            return new InvestigatorData().GetApplicationInfo(id);
        }

        public List<Application> GetEnrollmentHistory(string id, int userid)
        {
            // Set audit
            var audit = new Audit()
            {
                TimeStamp = DateTime.Now,
                UserId = userid,
                EventType = (int)AuditEnum.GetEnrolmentHistory,
                Description = "EnrolmentNo: " + id.Replace(",", ", ")
            };

            // Add audit trail
            new AuditData().AddAudit(audit);

            return new InvestigatorData().GetEnrollmentHistory(id);
        }

        public List<Person> GetAfisTransactions(string id, string formno, int userid)
        {
            // Set audit
            var desc = string.IsNullOrEmpty(id) ? ("Formno: " + formno) : ("Idperson: " + id);
            var audit = new Audit()
            {
                TimeStamp = DateTime.Now,
                UserId = userid,
                EventType = (int)AuditEnum.GetAfisTransactions,
                Description = desc
            };

            // Add audit trail
            new AuditData().AddAudit(audit);

            return new InvestigatorData().GetAfisTransactions(id, formno);
        }

        public Application GetTraceBooklet(string id, int userid)
        {
            // Set audit
            var audit = new Audit()
            {
                TimeStamp = DateTime.Now,
                UserId = userid,
                EventType = (int)AuditEnum.GetTraceBooklet,
                Description = "DocNo: " + id
            };

            // Add audit trail
            new AuditData().AddAudit(audit);

            return new InvestigatorData().GetTraceBooklet(id);
        }

        public Summary GetTableViewData(string id, string formno, int userid)
        {
            // Set audit
            var desc = string.IsNullOrEmpty(id) ? ("Formno: " + formno) : ("Idperson: " + id);
            var audit = new Audit
            {
                TimeStamp = DateTime.Now,
                UserId = userid,
                EventType = (int)AuditEnum.GetTableViewData,
                Description = desc
            };

            // Add audit trail
            new AuditData().AddAudit(audit);

            return new InvestigatorData().GetTableViewData(id, formno);
        }

        public List<PassportReport> GetPassportsIssued(DateTime start, DateTime end)
        {
            return new InvestigatorData().GetPassportsIssued(start, end);
        }

        public int ClearFingerprint()
        {
            try
            {
                System.IO.File.Delete(@"C:\Repository\Investigator\Fingerprints\fingerprint.wsq");
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int SaveFingerprint(byte[] fingerprint)
        {
            try
            {
                //System.IO.File.WriteAllBytes(@"C:\Repository\Investigator\Fingerprints\fingerprint.wsq", fingerprint);
                System.IO.File.WriteAllBytes(@"C:\Forensics\Fingerprints\fingerprint.wsq", fingerprint);
                return 1;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
