﻿using System;

namespace EPMS_Forensics_Entities
{
    public class LocEnrolProfile
    {
        public string Formno { get; set; }

        public int Appreason { get; set; }

        public string Branchcode { get; set; }

        public long Enrollocationid { get; set; }

        public long Layoutid { get; set; }

        public string Olddocno { get; set; }

        public string Idperson { get; set; }

        public string Doctype { get; set; }

        public string Fileno { get; set; }

        public int Nofinger { get; set; }

        public string Enrolby { get; set; }

        public DateTime Enroltime { get; set; }

        public string Appby { get; set; }

        public DateTime? Apptime { get; set; }

        public string Remarks { get; set; }

        public string Stagecode { get; set; }

        public int Priority { get; set; }

        public string Overwriteafisby { get; set; }

        public DateTime? Overwriteafistime { get; set; }

        public string Overwritepriorityby { get; set; }

        public DateTime? Overwriteprioritytime { get; set; }

        public string Overwriteadminrejectby { get; set; }

        public DateTime? Overwriteadminrejecttime { get; set; }

        public string Overwriteotherby { get; set; }

        public DateTime? Overwriteothertime { get; set; }

        public string Thirdpartyissueby { get; set; }

        public DateTime? Thirdpartyissuetime { get; set; }

        public string Issueby { get; set; }

        public DateTime? Issuetime { get; set; }

    }
}
