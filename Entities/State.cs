﻿namespace EPMS_Forensics_Entities
{
    public class State
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
