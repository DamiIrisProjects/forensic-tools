﻿using System.Collections.Generic;

namespace EPMS_Forensics_Entities
{
    public class BranchReport
    {
        public string ReportName { get; set; }

        public string ReportSubHeader { get; set; }

        public string Interval { get; set; }

        public string SubInterval { get; set; }

        public string TimeStamp { get; set; }

        public int Page { get; set; }

        public string HeaderName { get; set; }

        public Dictionary<string, int> OrdinaryPassports { get; set; }

        public Dictionary<string, int> DiplomaticPassports { get; set; }

        public Dictionary<string, int> OfficialPassports { get; set; }

        public int OrdinaryPassportsTotal { get; set; }

        public int DiplomaticPassportsTotal { get; set; }

        public int OfficialPassportsTotal { get; set; }
    }
}