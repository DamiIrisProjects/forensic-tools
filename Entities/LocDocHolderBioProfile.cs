﻿using System;

namespace EPMS_Forensics_Entities
{
    public class LocDocHolderBioProfile
    {
        public string Idperson { get; set; }

        public byte[] Faceimage { get; set; }

        public byte[] Faceimagej2K { get; set; }

        public DateTime? Faceentrytime { get; set; }

        public byte[] Finger1Image { get; set; }

        public string Finger1Code { get; set; }

        public int? Finger1Reason { get; set; }

        public byte[] Finger2Image { get; set; }

        public string Finger2Code { get; set; }

        public int? Finger2Reason { get; set; }

        public byte[] Finger3Image { get; set; }

        public string Finger3Code { get; set; }

        public int? Finger3Reason { get; set; }

        public byte[] Finger4Image { get; set; }

        public string Finger4Code { get; set; }

        public int? Finger4Reason { get; set; }

        public DateTime? Fingerentrytime { get; set; }

        public byte[] Signimage { get; set; }

        public DateTime? Signentrytime { get; set; }
    }
}
