﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities
{
    public class Booklet
    {
        [DataMember]
        public string DocNo { get; set; }

        [DataMember]
        public DateTime EntryTime { get; set; }

        [DataMember]
        public string ChipSn { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public DateTime TransferTime { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string BoxSn { get; set; }

        [DataMember]
        public string StageCode { get; set; }

        [DataMember]
        public string Color { get; set; }
    }
}
