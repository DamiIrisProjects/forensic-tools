﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities
{
    [DataContract]
    public class Audit
    {
        [DataMember]
        public int EventType { get; set; }

        [DataMember]
        public string EventTypeName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? UserId { get; set; }

        [DataMember]
        public DateTime TimeStamp { get; set; }

        [DataMember]
        public int? DocId { get; set; }

        [DataMember]
        public Person User { get; set; }
    }

    [DataContract]
    public enum AuditEnum
    {
        LoginSuccess = 1,
        LogOut = 2,
        Search = 3,
        View = 4,
        Download = 5,
        Delete = 6,
        Upload = 7,
        LoginFailed = 8,
        AccessLevelChange = 9,
        UpdatedProfile = 10,
        CreateEvent = 11,
        UpdateEvent = 12,
        DeleteEvent = 13,
        SearchPassportByApplicant = 14,
        SearchPassportByApplication = 15,
        SearchPassportByBooklet = 16,
        SearchPassportByFingerprint = 17,
        SearchBlacklist = 18,
        GetApplicationInformation = 19,
        GetEnrolmentHistory = 20,
        GetAfisTransactions = 21,
        GetTraceBooklet = 22,
        GetTableViewData = 23
    }
}
