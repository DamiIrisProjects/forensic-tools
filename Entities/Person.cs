﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities
{
    [DataContract]
    public class Person
    {
        public Person()
        {
            Applications = new List<Application>();
            AfisRecords = new List<AfistTransaction>();
            FormNoList = new List<string>();
        }

        [DataMember]
        public int PersonId { get; set; }

        [DataMember]
        public string Firstname { get; set; }

        [DataMember]
        public string Active { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Middlename { get; set; }

        [DataMember]
        public string NameString { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string Salt { get; set; }

        [DataMember]
        public string Hash { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public DateTime DateOfRegistration { get; set; }

        [DataMember]
        public DateTime Dob { get; set; }

        [DataMember]
        public int Counts { get; set; }

        [DataMember]
        public string StateOfOrigin { get; set; }

        [DataMember]
        public string PlaceOfBirth { get; set; }

        [DataMember]
        public List<string> FormNoList { get; set; }

        [DataMember]
        public string FormNo { get; set; }
        
        [DataMember]
        public string IdPerson { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string HomeAddress { get; set; }

        [DataMember]
        public byte[] Photo { get; set; }

        [DataMember]
        public byte[] Signature { get; set; }

        [DataMember]
        public List<Application> Applications { get; set; }

        [DataMember]
        public List<AfistTransaction> AfisRecords { get; set; }

        [DataMember]
        public string AppType { get; set; }

        [DataMember]
        public byte[] Fingerprint { get; set; }

        public string FullName => $"{UppercaseFirst(Firstname)} {UppercaseFirst(Surname)}".Trim();

        public string FirstNameUpper => UppercaseFirst(Firstname);

        public string SurnameUpper => UppercaseFirst(Surname);

        public string MiddlenameUpper => UppercaseFirst(Middlename);

        public string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            var a = s.ToLower().ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        } 
    }
}
