﻿using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities
{
    public class Branch
    {
        [DataMember]
        public string BranchCode { get; set; }

        [DataMember]
        public int BranchId { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public string Ip { get; set; }
    }
}
