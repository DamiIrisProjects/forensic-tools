﻿using System;

namespace EPMS_Forensics_Entities
{
    public class LocDocHolderMainProfile
    {
        public string Idperson { get; set; }

        public string Surname { get; set; }

        public string Firstname { get; set; }

        public DateTime? Birthdate { get; set; }

        public string Birthtown { get; set; }

        public string Birthstate { get; set; }

        public string Sex { get; set; }

        public string Nationality { get; set; }

        public string Personalno { get; set; }
    }
}
