﻿using System;

namespace EPMS_Forensics_Entities
{
    public class LocDocHolderCustomProfile
    {
        public string Idperson { get; set; }

        public string Othername1 { get; set; }

        public string Othername2 { get; set; }

        public string Othername3 { get; set; }

        public string Othername4 { get; set; }

        public int? Dobflag { get; set; }

        public string Originstate { get; set; }

        public string Addstreet { get; set; }

        public string Addtown { get; set; }

        public string Addstate { get; set; }

        public string Profession { get; set; }

        public string Occupation { get; set; }

        public int? Height { get; set; }

        public int? Eyecolor { get; set; }

        public int? Haircolor { get; set; }

        public int? Maritalstatus { get; set; }

        public string Birthmark { get; set; }

        public string Maidenname { get; set; }

        public string Nokname { get; set; }

        public string Nokaddress { get; set; }

        public string Child1Sname { get; set; }

        public string Child1Fname { get; set; }

        public DateTime? Child1Dob { get; set; }

        public string Child1Pobt { get; set; }

        public string Child1Pobs { get; set; }

        public string Child1Sex { get; set; }

        public string Child2Sname { get; set; }

        public string Child2Fname { get; set; }

        public DateTime? Child2Dob { get; set; }

        public string Child2Pobt { get; set; }

        public string Child2Pobs { get; set; }

        public string Child2Sex { get; set; }

        public string Child3Sname { get; set; }

        public string Child3Fname { get; set; }

        public DateTime? Child3Dob { get; set; }

        public string Child3Pobt { get; set; }

        public string Child3Pobs { get; set; }

        public string Child3Sex { get; set; }

        public string Child4Sname { get; set; }

        public string Child4Fname { get; set; }

        public DateTime? Child4Dob { get; set; }

        public string Child4Pobt { get; set; }

        public string Child4Pobs { get; set; }

        public string Child4Sex { get; set; }

        public string Title { get; set; }

        public string Eyecolorother { get; set; }

        public string Haircolorother { get; set; }

        public string Addpermanent { get; set; }

        public string Email { get; set; }

        public string Contactphone { get; set; }

        public string Mobilephone { get; set; }

        public string Gtoname { get; set; }

        public string Gtoaddress { get; set; }
    }
}
