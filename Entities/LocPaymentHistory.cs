﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPMS_Forensics_Entities
{
    public class LocPaymentHistory
    {
        public string Formno { get; set; }

        public string Bankdraftno { get; set; }

        public DateTime? Pymttime { get; set; }

        public string Pymtrecvby { get; set; }

        public int? Pymtamt { get; set; }

        public string Bankname { get; set; }

        public string Receiptno { get; set; }

        public string Refno { get; set; }

        public string Appid { get; set; }

    }
}
