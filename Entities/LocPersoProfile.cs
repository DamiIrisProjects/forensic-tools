﻿using System;

namespace EPMS_Forensics_Entities
{
    public class LocPersoProfile
    {
        public string Formno { get; set; }

        public string Docno { get; set; }

        public DateTime? Persotime { get; set; }

        public string Branchcode { get; set; }

        public string Stagecode { get; set; }

        public long? Persolocationid { get; set; }

        public long? Layoutid { get; set; }

        public string Doctype { get; set; }

        public DateTime? Issuedate { get; set; }

        public DateTime? Expirydate { get; set; }

        public int? Priority { get; set; }

        public string Remarks { get; set; }

        public string Chipsn { get; set; }
    }
}
