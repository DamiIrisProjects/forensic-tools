﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities
{
    public class AfistTransaction
    {
        [DataMember]
        public string RecordId { get; set; }

        [DataMember]
        public string DmlType { get; set; }

        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public string StageCode { get; set; }

        [DataMember]
        public DateTime AdtDate { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }

        [DataMember]
        public DateTime LastModified { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string OldDocNumber { get; set; }

        [DataMember]
        public string Flag1 { get; set; }

        [DataMember]
        public string Flag2 { get; set; }
    }
}
