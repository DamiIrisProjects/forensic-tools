﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities
{
    public class Application
    {
        public Application()
        {
            Nok = new Person();
            Booklets = new List<Booklet>();
        }

        [DataMember]
        public string Firstname { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Middlename { get; set; }

        [DataMember]
        public List<Booklet> Booklets { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string StageCode { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Flag { get; set; }

        [DataMember]
        public string ApplicationType { get; set; }

        [DataMember]
        public string DocumentType { get; set; }

        [DataMember]
        public string DocumentNo { get; set; }

        [DataMember]
        public string DocumentNoCheckDigit { get; set; }

        [DataMember]
        public string OldDocumentNo { get; set; }

        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public string QueryNo { get; set; }

        [DataMember]
        public string Appr { get; set; }

        [DataMember]
        public string ApprString { get; set; }

        [DataMember]
        public DateTime Dob { get; set; }

        [DataMember]
        public string DobCheckDigit { get; set; }

        [DataMember]
        public string StageDescription { get; set; }
        
        [DataMember]
        public DateTime EnrolDate { get; set; }

        [DataMember]
        public string IssuingBranch { get; set; }

        [DataMember]
        public DateTime IssueDate { get; set; }

        [DataMember]
        public DateTime ExpiryDate { get; set; }

        [DataMember]
        public string ExpiryDateCheckDigit { get; set; }

        [DataMember]
        public string Mrz1 { get; set; }

        [DataMember]
        public string Mrz2 { get; set; }


        [DataMember]
        public string CompositeCheckDigit { get; set; }

        [DataMember]
        public string FingerprintPosition { get; set; }

        [DataMember]
        public string Score { get; set; }

        [DataMember]
        public DateTime PersoDate { get; set; }

        [DataMember]
        public string EnrolledBy { get; set; }

        [DataMember]
        public DateTime EnrolledByTime { get; set; }

        [DataMember]
        public string ApprovedBy { get; set; }

        [DataMember]
        public DateTime ApprovedByTime { get; set; }

        [DataMember]
        public string EncodedBy { get; set; }

        [DataMember]
        public DateTime EncodedByTime { get; set; }

        [DataMember]
        public string PrintBy { get; set; }

        [DataMember]
        public DateTime PrintByTime { get; set; }

        [DataMember]
        public string QcBy { get; set; }

        [DataMember]
        public DateTime QcByTime { get; set; }

        [DataMember]
        public string Issuedby { get; set; }

        [DataMember]
        public DateTime IssuedbyTime { get; set; }

        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string PlaceOfBirth { get; set; }

        [DataMember]
        public string StateOfOrigin { get; set; }

        [DataMember]
        public string HomeAddress { get; set; }

        [DataMember]
        public string MobileNumber { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string ContactNumber { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public Person Nok { get; set; }


        [DataMember]
        public string GuarantorName { get; set; }

        [DataMember]
        public string GuarantorAdress { get; set; }

        public string FirstNameUpper => UppercaseFirst(Firstname);

        public string SurnameUpper => UppercaseFirst(Surname);

        public string MiddlenameUpper => UppercaseFirst(Middlename);

        public string IssuingBranchUpper
        {
            get 
            { 
                // For some reason, he has this hardcoded. Just renaming HQ and Gwagalada
                if (IssuingBranch.ToLower() == "fct abuja")
                    return "Gwagwalada FCT";

                if (IssuingBranch.ToLower() == "fct abuja")
                    return "Gwagwalada FCT";

                return UppercaseFirst(IssuingBranch); 
            }
        }

        public string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            var a = s.ToLower().ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        } 
    }
}
