﻿using System;

namespace EPMS_Forensics_Entities
{
    public class LocDocProfile
    {
        public string Docno { get; set; }

        public string Formno { get; set; }

        public string Doctype { get; set; }

        public DateTime? Expirydate { get; set; }

        public DateTime? Issuedate { get; set; }

        public string Issueplace { get; set; }

        public DateTime? Printtime { get; set; }

        public string Printby { get; set; }

        public DateTime? Encodetime { get; set; }

        public string Encodeby { get; set; }

        public DateTime? Qctime { get; set; }

        public string Qcby { get; set; }

        public DateTime? Issuetime { get; set; }

        public string Issueby { get; set; }

        public string Authoritycode { get; set; }

        public string Acqbatch { get; set; }

        public string Branchcode { get; set; }

        public string Stagecode { get; set; }
    }
}
