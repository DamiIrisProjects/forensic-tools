﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities
{
    [DataContract]
    public class PassportReport
    {
        [DataMember]
        public string DocType { get; set; }

        [DataMember]
        public int BranchCode { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public int IsForeign { get; set; }

        [DataMember]
        public DateTime IssueDay { get; set; }

        [DataMember]
        public int NumberIssued { get; set; }

    }
}
