﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities.Summary
{
    public class EnrolmentData
    {
        [DataMember]
        public string Remarks { get; set; }

        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public string DocType { get; set; }

        [DataMember]
        public string OldDocNo { get; set; }

        [DataMember]
        public string OldFormNo { get; set; }

        [DataMember]
        public string StageCode { get; set; }

        [DataMember]
        public DateTime EnrolTime { get; set; }

        [DataMember]
        public string EnrolBy { get; set; }
    }
}