﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities.Summary
{
    public class PersonalData
    {
        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Firstname { get; set; }

        [DataMember]
        public string Middle { get; set; }

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public DateTime BirthDate { get; set; }

        [DataMember]
        public string StateOfOrigin { get; set; }

        [DataMember]
        public string PlaceOfBirth { get; set; }

        [DataMember]
        public string FormNo { get; set; }
    }
}
