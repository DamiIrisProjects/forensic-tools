﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities.Summary
{
    public class Summary
    {
        public Summary()
        {
            AfisData = new List<AfisData>();
            DocumentInfo = new List<DocumentInfo>();
            EnrolmentData = new List<EnrolmentData>();
            FingerprintSubmission = new List<FingerprintSubmission>();
            PersonalData = new List<PersonalData>();
            PersonalisationInfo = new List<PersonalisationInfo>();
        }

        [DataMember]
        public List<AfisData> AfisData { get; set; }

        [DataMember]
        public List<DocumentInfo> DocumentInfo { get; set; }

        [DataMember]
        public List<EnrolmentData> EnrolmentData { get; set; }

        [DataMember]
        public List<FingerprintSubmission> FingerprintSubmission { get; set; }

        [DataMember]
        public List<PersonalData> PersonalData { get; set; }

        [DataMember]
        public List<PersonalisationInfo> PersonalisationInfo { get; set; }
    }
}
