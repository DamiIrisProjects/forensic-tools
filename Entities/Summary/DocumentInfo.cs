﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities.Summary
{
    public class DocumentInfo
    {
        [DataMember]
        public string DocType { get; set; }

        [DataMember]
        public string DocNo { get; set; }

        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public string StageCode { get; set; }

        [DataMember]
        public DateTime IssueTime { get; set; }
       
        [DataMember]
        public string IssueBy { get; set; }
    }
}