﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities.Summary
{
    public class PersonalisationInfo
    {
        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public string DocNo { get; set; }

        [DataMember]
        public string StageCode { get; set; }

        [DataMember]
        public DateTime PersoTime { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}