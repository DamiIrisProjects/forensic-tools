﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities.Summary
{
    public class FingerprintSubmission
    {
        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public string QueryNo { get; set; }

        [DataMember]
        public string AppReason { get; set; }

        [DataMember]
        public string Flag { get; set; }

        [DataMember]
        public string StageCode { get; set; }

        [DataMember]
        public DateTime EntryTime { get; set; }

        [DataMember]
        public string OldDocNo { get; set; }

        [DataMember]
        public string DocType { get; set; }

        [DataMember]
        public string Remarks { get; set; }
    }
}