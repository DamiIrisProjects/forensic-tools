﻿using System;
using System.Runtime.Serialization;

namespace EPMS_Forensics_Entities.Summary
{
    public class AfisData
    {
        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public string QueryNo { get; set; }

        [DataMember]
        public DateTime CreationDate { get; set; }

        [DataMember]
        public DateTime LastModified { get; set; }
    }
}
