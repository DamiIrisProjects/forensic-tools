﻿namespace EPMS_Forensics_Entities
{
    public class LocPassport
    {
        public LocDocHolderBioProfile LocDocHolderBioProfile { get; set; }

        public LocDocHolderCustomProfile LocDocHolderCustomProfile { get; set; }

        public LocDocHolderMainProfile LocDocHolderMainProfile { get; set; }

        public LocDocProfile LocDocProfile { get; set; }

        public LocEnrolProfile LocEnrolProfile { get; set; }

        public LocPersoProfile LocPersoProfile { get; set; }

        public LocPaymentHistory LocPaymentHistory { get; set; }
    }
}
