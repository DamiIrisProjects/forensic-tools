﻿using System;

namespace EPMS_Forensics_Entities
{
    static public class Helper
    {
        public static Error CreateError(Exception ex)
        {
            var error = new Error()
            {
                Message = ex.Message,
                StackTrace = ex.StackTrace,
                TargetSite = ex.TargetSite.Name,
                Timestamp = DateTime.Now
            };

            if (ex.InnerException != null)
            {
                error.InnerExMsg = ex.InnerException.Message;
                error.InnerExSrc = ex.InnerException.Source;
                error.InnerExStkTrace = ex.InnerException.StackTrace;
                error.InnerExTargetSite = ex.InnerException.TargetSite.Name;
            }

            return error;
        }

        public static string ToFirstLetterUpper(string word)
        {
            if (word != string.Empty && word != " ")
            {
                var a = word.ToLower().ToCharArray();
                a[0] = char.ToUpper(a[0]);

                return new string(a);
            }

            return string.Empty;
        }
    }
}
