﻿using EPMS_Forensics_Entities;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Globalization;
using System.Net.Mail;
using System.Text;

namespace EPMS_Forensics_Data
{
    public class AirportLoginData : OracleDataProv
    {
        public Person GetUserDetails(string email)
        {
            using (var dset = new DataSet())
            {
                // Get hash and salt from database
                SetConnectionString("OracleConnectionStringIdoc");
                var query = @"select user_id, firstname, surname, email, gender, date_created, is_admin, phone_number, user_id, pwd_salt, pwd_hash, is_active, last_edit, clearance_level, profile_pic, u.position_id, u.department_id, position, department
                            from airport_user u left join list_staff_positions lp on u.position_id = lp.position_id left join list_departments d on u.department_id = d.department_id where email = :email and u.is_active = 1";

                OracleParameter[] param = { new OracleParameter("email", email.ToUpper()) };
                RunProcedure(query, param, dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    var row = dset.Tables[0].Rows[0];
                    var user = new Person();

                    user.PersonId = int.Parse(row["user_id"].ToString());
                    user.Firstname = row["firstname"].ToString();
                    user.Gender = row["gender"].ToString();
                    user.PhoneNumber = row["phone_number"].ToString();
                    user.Surname = row["surname"].ToString();
                    user.EmailAddress = row["email"].ToString();
                    if (!string.IsNullOrEmpty(row["date_created"].ToString()))
                        user.DateOfRegistration = DateTime.Parse(row["date_created"].ToString());
                    user.Salt = row["pwd_salt"].ToString();
                    user.Hash = row["pwd_hash"].ToString();

                    return user;
                }
                else
                    return null;
            }
        }

        public int VerifyUsernameDoesNotExists(string firstname, string surname, string phonenumber, string email)
        {
            SetConnectionString("OracleConnectionStringIdoc");
            var dset = new DataSet();

            var query = "select email, is_active, registration_guid from airport_user where email = :email";

            OracleParameter[] param =
                    {   
                        new OracleParameter("email",  email.ToUpper()),
                    };

            RunProcedure(query, param, dset);
            if (dset.Tables[0].Rows.Count != 0)
            {
                // User already exists. Now if the user hasn't been activated already, send them an activation email
                if (dset.Tables[0].Rows[0]["is_active"].ToString() == "0")
                {
                    SendUserActivationMail(firstname, surname, phonenumber, email, dset.Tables[0].Rows[0]["registration_guid"].ToString(), 2);
                    return 1;
                }
                else
                    return 2;

            }
            else
                return 0;
        }

        public int VerifyUsernameExists(string email)
        {
            SetConnectionString("OracleConnectionStringIdoc");
            var dset = new DataSet();

            var query = "select email, is_active, registration_guid from airport_user where email = :email";

            OracleParameter[] param =
                    {
                        new OracleParameter("email",  email.ToUpper()),
                    };

            RunProcedure(query, param, dset);
            if (dset.Tables[0].Rows.Count != 0)
            {
                // User already exists. Now if the user hasn't been activated already, send them an activation email
                if (dset.Tables[0].Rows[0]["is_active"].ToString() == "0")
                {
                    return 1;
                }
                else
                    return 2;

            }
            else
                return 0;
        }

        public int RegisterUser(string firstname, string surname, string phonenumber, string email, string password)
        {
            SetConnectionString("OracleConnectionStringIdoc");

            // Hash password
            var sh = SaltedHash.Create(password);

            var salt = sh.Salt;
            var hash = sh.Hash;
            var regGuid = Guid.NewGuid().ToString();

            var query = @"insert into airport_user (firstname, phone_number, surname, email, PWD_HASH, PWD_SALT, date_created, registration_guid) values (:firstname, :phone_number, :surname, :email, :PWD_HASH, :PWD_SALT, :date_created, :registration_guid)";
            OracleParameter[] param =
                {   
                    new OracleParameter("firstname",  firstname.ToUpper()),
                    new OracleParameter("surname",  surname.ToUpper()),
                    new OracleParameter("phone_number",  phonenumber),
                    new OracleParameter("email",  email.ToUpper()),
                    new OracleParameter("PWD_HASH",  hash),
                    new OracleParameter("PWD_SALT",  salt),
                    new OracleParameter("date_created",  DateTime.Now),
                    new OracleParameter("registration_guid",  regGuid)
                };

            var result = ExecuteQuery(query, param);

            if (result != 0)
            {
                SendUserActivationMail(firstname, surname, phonenumber, email, regGuid, 1);
            }

            return 4;
        }

        public int ActivateUser(string guid)
        {
            SetConnectionString("OracleConnectionStringIdoc");
            var query = "update airport_user set is_active = 1 where registration_guid = :registration_guid";
            OracleParameter[] param =
                {   
                    new OracleParameter("registration_guid",  guid)
                };

            ExecuteQuery(query, param);

            query = "select email from airport_user where registration_guid = :registration_guid";
            OracleParameter[] paramx =
               {
                    new OracleParameter("registration_guid",  guid)
                };

            var email = ExecuteScalarStr(query, paramx);
            SendUserActivatedMail(email);

            return 1;
        }

        public int ResetUserPassword(string email)
        {
            var res = VerifyUsernameExists(email);

            // If the user exists
            if (res == 2)
            {
                SendPasswordByMail(email);
                return 0;
            }
            else
                return 2;
        }

        public int UpdateForgottenPassword(string password, string guid)
        {
            SetConnectionString("OracleConnectionStringIdoc");

            // Hash password
            var sh = SaltedHash.Create(password);

            var salt = sh.Salt;
            var hash = sh.Hash;

            // Update Password
            var query = "update airport_user set PWD_HASH = :PWD_HASH, pwd_salt = :pwd_salt where new_key = :guid";

            OracleParameter[] param =
                    {   
                        new OracleParameter("PWD_HASH", hash),
                        new OracleParameter("pwd_salt",  salt),
                        new OracleParameter("guid",  guid),
                    };

            ExecuteQuery(query, param);

            // Update member is_changingpw
            query = "update airport_user set is_changing_pw = 0 where new_key = :guid";

            OracleParameter[] param2 =
                    {   
                        new OracleParameter("guid",  guid),
                    };

            ExecuteQuery(query, param2);

            return 1;
        }

        public int VerifyPassword(string email, string password)
        {
            // Get hash and salt from database
            SetConnectionString("OracleConnectionStringIdoc");
            var query = @"select email, pwd_salt, pwd_hash from airport_user where email = :email";

            OracleParameter[] param = { new OracleParameter("email", email.ToUpper()) };
            var dset = new DataSet();

            RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];
                var salt = row["pwd_salt"].ToString();
                var hash = row["pwd_hash"].ToString();

                // Verify
                var sh = SaltedHash.Create(salt, hash);
                var value = sh.Verify(password);

                if (value) return 1;

                return 2;
            }
            else
                return 0;
        }

        public int VerifyPasswordChange(string guid)
        {
            var query = "select is_changing_pw, new_key, last_pwchange_expiry from airport_user where new_key = :guid";

            SetConnectionString("OracleConnectionStringIdoc");
            var dset = new DataSet();

            OracleParameter[] param =
                        {   
                            new OracleParameter("guid",  guid)
                        };

            RunProcedure(query, param, dset);

            // Verify if its correct
            if (dset.Tables[0].Rows.Count != 0)
            {
                var pwstatus = int.Parse(dset.Tables[0].Rows[0]["is_changing_pw"].ToString());
                var expiry = DateTime.Parse((dset.Tables[0].Rows[0]["last_pwchange_expiry"].ToString()));

                // Check password status is set to changing
                if (pwstatus == 1)
                {
                    // Check Expiration
                    if (DateTime.Now.CompareTo(expiry) == 1)
                        return 2;
                    else
                        return 0;
                }
                else
                    return 1;
            }
            else
                return 1;
        }

        public string SendPasswordByMail(string email)
        {
            SetConnectionString("OracleConnectionStringIdoc");
            var newKey = Guid.NewGuid();

            //Set guid and pw status
            var query = "update airport_user set is_changing_pw = 1, new_key = :new_key, last_pwchange_expiry = :last_pwchange_expiry where email = :email";

            OracleParameter[] param2 =
                    {   
                        new OracleParameter("new_key",  newKey.ToString()),
                        new OracleParameter("email",  email.ToUpper()),
                        new OracleParameter("last_pwchange_expiry",  DateTime.Now.AddHours(1))
                    };
            ExecuteQuery(query, param2);

            SendPasswordMail(email, newKey.ToString());
            return string.Empty;

        }

        private void SendPasswordMail(string email, string guid)
        {
            var mail = new MailMessage();
            const string iris = "NoReply@irissmart.com.ng";
            const string friendlyName = "Iris Smart Technologies";
            var server = ConfigurationManager.AppSettings["serverurl"] == null ? "http://10.10.10.50:451" 
                : ConfigurationManager.AppSettings["serverurl"];

            var buttonstyle = "background-color: #336699; font-family: arial; text-align:center; width:180px; height:30px; color: White; margin: 15px 0px 20px 0px; border: none; cursor: pointer; line-height: 30px; font-size: 15px; border-radius: 2px 2px 2px 2px; -moz-border-radius: 2px 2px 2px 2px; -webkit-border-radius: 2px 2px 2px 2px; -khtml-border-radius:  2px 2px 2px 2px;";
            var button = "<a style='text-decoration: none' href=\"" + server + "?reset=" + guid + "&usr=" + email + "\"><div Style='" + buttonstyle + "'>Reset My Password</div></a>";

            var sb = new StringBuilder();
            sb.Append("<div style='color:#333'>Hello, </div><br/>");
            sb.Append("<div style='color:#333'>Following your request on the Iris website, here is a link from which you can change your password:</div><br/>");
            sb.Append(button);
            sb.Append("<div style='color:#333; margin: 0px 0px 15px 0px'><b>Note:</b> You must be on the Iris network for this to work</div><br/>");
            sb.Append("<div style='color:#333; margin: 0px 0px 10px 0px'>Sincerly,</div><br/>");
            sb.Append("<div style='color:#333'>Iris Software Team</div>");

            mail.From = new MailAddress(iris, friendlyName);
            mail.To.Add(email);

            //set the content
            mail.Subject = "Password Recovery";
            mail.Body = sb.ToString();
            mail.IsBodyHtml = true;

            SendMail(mail);
        }

        public void SendUserActivationMail(string firstname, string surname, string phonenumber, string email, string guid, int type)
        {
            var mail = new MailMessage();

            var activationmail = ConfigurationManager.AppSettings["ActivationEmail"];
            var server = ConfigurationManager.AppSettings["serverurl"];
            var iris = ConfigurationManager.AppSettings["CompanyEmail"];
            var friendlyName = ConfigurationManager.AppSettings["FriendlyName"];
            var buttonstyle = "background-color: #336699; font-family: arial; text-align:center; width:150px; " +
                                "height:30px; color: White; margin: 15px 0px 20px 0px; border: none; cursor: pointer; " +
                                "line-height: 30px; font-size: 16px; border-radius: 2px 2px 2px 2px; -moz-border-radius: 2px 2px 2px 2px; " +
                                "-webkit-border-radius: 2px 2px 2px 2px; -khtml-border-radius:  2px 2px 2px 2px;";
            var button = "<a href='" + server + "?guid=" + guid + "'><div Style='" + buttonstyle + "'>Activate</div></a>";

            var sb = new StringBuilder();
            sb.Append("<div style='color:#333'>");
            sb.Append("<div style='margin-bottom: 10px;'>Hello,</div>");
            sb.Append("<div style='margin-bottom: 10px;'>A request to register was made. The details are as follows:</div>");
            sb.Append($"<div>Firstname: {CultureInfo.CurrentCulture.TextInfo.ToTitleCase(firstname.ToLower())}</div>");
            sb.Append($"<div>Surname: {CultureInfo.CurrentCulture.TextInfo.ToTitleCase(surname.ToLower())}</div>");
            sb.Append($"<div>Phonenumber: {phonenumber}</div>");
            sb.Append($"<div style='margin-bottom: 10px;'>Email: {email}</div>");
            sb.Append("<div style='margin-bottom: 10px;'>Please click on the link below to activate the user:</div>");
            sb.Append(button);
            sb.Append("<div style='margin-bottom: 10px;'>Sincerly,</div>");
            sb.Append("<div>Iris Software Team</div>");
            sb.Append("</div>");

            mail.From = new MailAddress(iris, friendlyName);
            mail.To.Add(activationmail);

            //set the content
            mail.Subject = "Airport Registration Request";

            if (type == 2)
                mail.Subject = "Reminder: Airport Registration Request";
            mail.Body = sb.ToString();
            mail.IsBodyHtml = true;

            SendMail(mail);
        }

        public void SendUserActivatedMail(string email)
        {
            var mail = new MailMessage();

            var iris = ConfigurationManager.AppSettings["CompanyEmail"];
            var friendlyName = ConfigurationManager.AppSettings["FriendlyName"];

            var sb = new StringBuilder();
            sb.Append("<div style='color:#333'>");
            sb.Append("<div style='margin-bottom: 10px;'>Hello,</div>");
            sb.Append("<div style='margin-bottom: 10px;'>Your request to register on the Forensic Portal has been accepted " +
                      "and your account has been activated. Please log in with your registered email and password.</div>");
            sb.Append("<div style='margin-bottom: 10px;'>Sincerly,</div>");
            sb.Append("<div>Iris Software Team</div>");
            sb.Append("</div>");

            mail.From = new MailAddress(iris, friendlyName);
            mail.To.Add(email);

            //set the content
            mail.Subject = "Forensic Identity Registration Request";
            
            mail.Body = sb.ToString();
            mail.IsBodyHtml = true;

            SendMail(mail);
        }

        private void SendMail(MailMessage msg)
        {
            try
            {
                var smtp = new SmtpClient();
                smtp.SendCompleted += SendCompletedCallback;
                smtp.SendAsync(msg, msg);
            }
            catch (Exception ex)
            {
                var error = Helper.CreateError(ex);
                new ErrorData().SaveError(error);
            }
        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            if (e.Cancelled)
            {

            }
            if (e.Error != null)
            {

            }
        }
    }
}
