﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;

namespace EPMS_Forensics_Data
{
    public class OracleDataProv
    {
        private string _connectionString = ConfigurationManager.ConnectionStrings["OracleConnectionStringPassport"].ToString();
        private DbConnection _connection;
        private DbCommand _command;

        protected void SetConnectionString(string conn)
        {
            _connectionString = ConfigurationManager.ConnectionStrings[conn].ToString();
        }


        private void CreateConnection()
        {
            if (_connection != null) return;

            try
            {
                _connection = new OracleConnection(_connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateTransactionCommand()
        {
            if (_command?.Transaction?.Connection != null && _command.Transaction.Connection.State == ConnectionState.Open)
                _command.Transaction.Connection.Close();

            if (_connection == null)
                CreateConnection();

            if (_connection.State == ConnectionState.Closed)
                _connection.Open();

            var transaction = _connection.BeginTransaction();

            _command = _connection.CreateCommand();
            _command.Transaction = transaction;
        }

        public int ExecuteQuery(OracleParameter[] parameters)
        {
            var cmd = _command;

            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            if (parameters != null && parameters.Length > 0)
            {
                cmd.Parameters.Clear();

                foreach (var p in parameters)
                {
                    if (p != null)
                        cmd.Parameters.Add(p);
                }
            }

            var iRowsAffected = cmd.ExecuteNonQuery();

            return iRowsAffected;
        }

        public int ExecuteScalar(string strQuery)
        {
            var cmd = _command;
            _command.CommandText = strQuery;

            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            var iRowsAffected = (int)cmd.ExecuteScalar();

            return iRowsAffected;
        }

        public string ExecuteScalarStr(string strQuery, OracleParameter[] parameters)
        {
            string iRowsAffected;

            try
            {
                if (_connection == null)
                {
                    CreateConnection();
                }

                if (_connection.State != ConnectionState.Open)
                    _connection.Open();

                using (var cmd = (OracleCommand)_connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        foreach (var p in parameters)
                        {
                            if (p.Value == null)
                                p.Value = DBNull.Value;

                            cmd.Parameters.Add(p);
                        }
                    }

                    cmd.Connection = (OracleConnection)_connection;
                    iRowsAffected = cmd.ExecuteScalar().ToString();
                }
            }
            finally
            {
                _connection?.Close();
            }
            return iRowsAffected;
        }

        public int ExecuteQuery(string strQuery, OracleParameter[] parameters)
        {
            int iRowsAffected;

            try
            {
                if (_connection == null)
                {
                    CreateConnection();
                }

                if (_connection.State != ConnectionState.Open)
                    _connection.Open();

                using (var cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (var i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = _connection;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                _connection?.Close();
            }
            return iRowsAffected;
        }

        public void RunProcedure(string strQuery, OracleParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (_connection == null)
                {
                    CreateConnection();
                }

                if (_connection.State != ConnectionState.Open)
                    _connection.Open();

                using (var cmd = _connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        foreach (var p in parameters)
                        {
                            cmd.Parameters.Add(p);
                        }
                    }

                    using (DbDataAdapter oracleDa = new OracleDataAdapter())
                    {
                        oracleDa.SelectCommand = cmd;
                        oracleDa.Fill(dataSet);
                        _connection.Close();
                    }
                }
            }
            finally
            {
                if (_connection?.State == ConnectionState.Open)
                {
                    _connection.Close();
                }
            }
        }

        public void RunStoredProcedure(OracleParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (_connection == null)
                {
                    CreateConnection();
                }

                if (_connection.State != ConnectionState.Open)
                    _connection.Open();

                using (var cmd = new OracleCommand())
                {
                    cmd.Connection = (OracleConnection)_connection;
                    cmd.CommandText = "launch_docholder_txtsearch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddRange(parameters);

                    cmd.ExecuteNonQuery();

                    // Now query the temp table
                    cmd.CommandText = "select * from docholder_txtsearch_result";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Clear();

                    using (DbDataAdapter oracleDa = new OracleDataAdapter())
                    {
                        oracleDa.SelectCommand = cmd;
                        oracleDa.Fill(dataSet);
                        _connection.Close();
                    }
                }
            }
            finally
            {
                if (_connection?.State == ConnectionState.Open)
                {
                    _connection.Close();
                }
            }
        }

        public int GetIdNumber(DbCommand cmd)
        {
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            cmd.CommandText = "select last_insert_id();";
            return Convert.ToInt32(cmd.ExecuteScalar());
        }

        public void CloseQuery()
        {
            _connection.Close();
        }
    }
}
