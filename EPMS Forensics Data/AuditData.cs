﻿using EPMS_Forensics_Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;

namespace EPMS_Forensics_Data
{
    public class AuditData: OracleDataProv
    {
        public List<Audit> GetAuditTrail(DateTime start, DateTime end)
        {
            var dset = new DataSet();
            var query = @"SELECT audit_id, event_type_name, event_type, user_id, time_stamp, doc_id, description from audits a 
                            left join list_event_types l on l.event_type_id = a.event_type
                            where time_stamp > TO_DATE('" + start.AddDays(-1).ToString("yyyy-MM-dd") + @"', 'YYYY-MM-DD')
                            and time_stamp < TO_DATE('" + end.AddDays(1).AddDays(1).ToString("yyyy-MM-dd") + @"', 'YYYY-MM-DD')";

            RunProcedure(query, null, dset);

            var audits = new List<Audit>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                var audit = new Audit
                {
                    EventType = int.Parse(row["event_type"].ToString()),
                    EventTypeName = row["event_type_name"].ToString(),
                    Description = row["description"].ToString(),
                    TimeStamp = DateTime.Parse(row["time_stamp"].ToString()),
                    User = string.IsNullOrEmpty(row["user_id"].ToString()) ? null : GetUserFromId(int.Parse(row["user_id"].ToString()))
                };

                // spaces for austerity sake
                audit.EventTypeName = string.Concat(audit.EventTypeName.Select(x => char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');
                audits.Add(audit);
            }

            return audits;
        }

        public void AddAudit(Audit audit)
        {
            SetConnectionString("OracleConnectionStringIdoc");
            var query = "insert into audits (event_type, user_id, time_stamp, doc_id, description) values (:event_type, :user_id, :time_stamp, :doc_id, :description)";
            OracleParameter[] param =
                    {   
                        new OracleParameter("event_type",  audit.EventType),
                        new OracleParameter("user_id",  audit.UserId == 0 ? null : audit.UserId),
                        new OracleParameter("time_stamp",  DateTime.Now),
                        new OracleParameter("description",  audit.Description),
                        new OracleParameter("doc_id",  audit.DocId == 0 ? null : audit.DocId)
                    };

            ExecuteQuery(query, param);
        }

        private Person GetUserFromId(int userId)
        {
            SetConnectionString("OracleConnectionStringIdoc");
            var dset = new DataSet();
            Person user = null;

            var query = @"select user_id, firstname, surname, email, gender, date_created, phone_number, user_id, pwd_salt, pwd_hash, is_active, last_edit, clearance_level, profile_pic, u.position_id, u.department_id, position, department
                            from iris_user u left join list_staff_positions lp on u.position_id = lp.position_id 
                            left join list_departments d on u.department_id = d.department_id where user_id = :user_id";
            OracleParameter[] param =
                {   
                    new OracleParameter("user_id",  userId)
                };

            RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                var row = dset.Tables[0].Rows[0];
                user = new Person
                {
                    PersonId = int.Parse(row["user_id"].ToString()),
                    Firstname = row["firstname"].ToString(),
                    Gender = row["gender"].ToString(),
                    PhoneNumber = row["phone_number"].ToString(),
                    Surname = row["surname"].ToString(),
                    EmailAddress = row["email"].ToString()
                };

                if (!string.IsNullOrEmpty(row["date_created"].ToString()))
                    user.DateOfRegistration = DateTime.Parse(row["date_created"].ToString());
                user.Salt = row["pwd_salt"].ToString();
                user.Hash = row["pwd_hash"].ToString();
            }

            return user;
        }
    }
}
