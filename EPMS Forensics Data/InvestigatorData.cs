﻿using EPMS_Forensics_Entities;
using EPMS_Forensics_Entities.Summary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace EPMS_Forensics_Data
{
    public class InvestigatorData : OracleDataProv
    {
        public List<Person> SearchForPassportHolder(string searchstring, List<OracleParameter> param, string formno, string oldformno)
        {
            var result = new List<Person>();

            var query = @"SELECT aq.formno, ep.doctype, ep.formno as epformno, aq.oldformno, aq.queryno, aq.olddocno, mp.surname, mp.firstname, cp.othername1, mp.sex, mp.birthdate, ep.idperson
                            FROM Enrolprofile ep
                            LEFT JOIN AfisQuery aq 
                            ON aq.formno = ep.formno
                            LEFT JOIN DocHolderMainProfile mp
                            ON ep.idperson = mp.idperson
                            LEFT JOIN DocHolderCustomProfile cp
                            ON ep.idperson = cp.idperson " +
                            searchstring +
                            " ORDER BY surname, firstname, othername1";

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                if (dset.Tables[0].Rows.Count != 0)
                {
                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var previous = result.FirstOrDefault(x => x.IdPerson == row["idperson"].ToString());

                        // Add the form number if its the same person
                        if (previous != null)
                        {
                            if (string.IsNullOrEmpty(row["formno"].ToString()))
                            {
                                if (previous.FormNoList.FirstOrDefault(x => x == row["epformno"].ToString()) == null)
                                    previous.FormNoList.Add(row["epformno"].ToString());
                            }
                            else
                            {
                                if (previous.FormNoList.FirstOrDefault(x => x == row["formno"].ToString()) == null)
                                    previous.FormNoList.Add(row["formno"].ToString());
                            }
                        }
                        else
                        {
                            var newperson = CreatePerson(row);

                            // if its passport number used to search, get any other form linked via the olddocno
                            if (param[0].ParameterName == "docno")
                            {
                                // If it has an olddocno
                                if (!string.IsNullOrEmpty(row["olddocno"].ToString()))
                                {
                                    newperson.FormNoList.AddRange(GetFormnosFromSearchingByOldDocNo(row["olddocno"].ToString()));
                                }

                                // Also check if there are any applications that have it as an olddoc
                                var olddocs = GetFormnosFromSearchingForDocNo(param[0].Value.ToString());

                                // Merge lists and prevent duplications
                                newperson.FormNoList = newperson.FormNoList.Union(olddocs).ToList();
                            }
                            // Get any other forms linked as well
                            else if (param[0].ParameterName == "formno")
                            {
                                // If it has an formno
                                if (!string.IsNullOrEmpty(row["oldformno"].ToString()))
                                {
                                    newperson.FormNoList.AddRange(GetFormnosFromSearchingByOldFormNo(row["oldformno"].ToString()));
                                }

                                // Also check if there are any applications that have it as an oldformno
                                var oldforms = GetFormnosFromSearchingForFormNo(param[0].Value.ToString());

                                // Merge lists and prevent duplications
                                newperson.FormNoList = newperson.FormNoList.Union(oldforms).ToList();
                            }


                            result.Add(newperson);
                        }
                    }

                    // Add any formno that is linked
                    if (!string.IsNullOrEmpty(oldformno) && !string.IsNullOrEmpty(formno))
                    {
                        var toAdd = result.FirstOrDefault(x => x.FormNoList.Contains(oldformno));

                        if (toAdd != null)
                        {
                            if (toAdd.FormNoList.FirstOrDefault(x => x == formno) == null)
                                toAdd.FormNoList.Add(formno);
                        }
                    }

                }
                else
                {
                    // Check if perhaps person is in afisquery but not yet in enrolprofile
                    query = @"select queryno, formno, a.doctype, oldformno, a.stagecode, dt.description, entrytime, s.description as stagecodedesc from afisquery a
                                left join lookup_doctype dt on dt.doctype = a.doctype
                                left join stage s on s.stagecode = a.stagecode
                                where formno = :formno";

                    using (var dset2 = new DataSet())
                    {
                        var param2 = new List<OracleParameter>
                        {
                            new OracleParameter("formno", param[0].Value)
                        };

                        new OracleDataProv().RunProcedure(query, param2.ToArray(), dset2);

                        foreach (DataRow row in dset2.Tables[0].Rows)
                        {
                            // Check if the person has a previous passport
                            if (!string.IsNullOrEmpty(row["oldformno"].ToString()))
                            {
                                var param3 =
                                    new List<OracleParameter>
                                    {
                                        new OracleParameter("formno", row["oldformno"].ToString())
                                    };
                                var newq = "WHERE ep.idperson IN (SELECT idperson FROM enrolprofile WHERE formno = :formno)";
                                return SearchForPassportHolder(newq, param3, row["formno"].ToString(), row["oldformno"].ToString());
                            }

                            // Otherwise
                            var person = new Person();
                            var app = new Application();
                            person.Firstname = "Pending";
                            person.Surname = "Pending";
                            person.Gender = "Pending";
                            person.Counts = 1;
                            person.Dob = DateTime.MinValue;
                            person.IdPerson = "Pending";
                            person.FormNo = row["formno"].ToString();
                            person.FormNoList.Add(row["formno"].ToString());
                            app.QueryNo = row["queryno"].ToString();
                            app.StageCode = row["stagecode"].ToString();
                            app.StageDescription = row["stagecodedesc"].ToString();
                            app.DocumentType = row["doctype"] + " (" + Helper.ToFirstLetterUpper(row["description"].ToString()) + ")";
                            app.EnrolDate = TryParseDateTime(row["entrytime"].ToString());
                            person.Applications.Add(app);
                            result.Add(person);
                        }
                    }
                }
            }

            return result;
        }

        public Application GetTraceBooklet(string id)
        {
            var result = new Application();
            var prefix = GetFirstLetters(id);
            var idnum = int.Parse(id.Replace(prefix, ""));

            var query = @"select chipsn, docno, entrytime, branchname, transfertime, i.stagecode, s.description as stagecodedesc, i.doctype, dt.description, boxsn, docpage from docinventory i
                                left join branch b on b.branchcode = i.branchcode
                                LEFT JOIN  lookup_doctype dt
                                on dt.doctype = i.doctype
                                LEFT JOIN  stage s
                                on s.stagecode = i.stagecode
                                where docno > :idmin and docno < :idmax";

            var param = new List<OracleParameter>()
            {
                new OracleParameter("idmin", prefix + (idnum - 6).ToString().PadLeft(8, '0')),
                new OracleParameter("idmax", prefix + (idnum + 6).ToString().PadLeft(8, '0'))
            };

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var booklet = new Booklet
                    {
                        ChipSn = row["chipsn"].ToString(),
                        DocNo = row["docno"].ToString(),
                        EntryTime = TryParseDateTime(row["entrytime"].ToString()),
                        BranchName = row["branchname"].ToString(),
                        TransferTime = TryParseDateTime(row["transfertime"].ToString()),
                        Type = Helper.ToFirstLetterUpper(row["description"].ToString().Replace(" PASSPORT", "")),
                        BoxSn = row["boxsn"].ToString(),
                        StageCode = row["stagecode"].ToString(),
                        Color = string.Empty
                    };

                    // Set color for report
                    if (booklet.StageCode == "EM5000")
                        booklet.Color = "validbooklet";
                    else if (booklet.StageCode == "IM1000")
                        booklet.Color = "documentarrived";
                    else if (booklet.StageCode == "IM2000")
                        booklet.Color = "senttobranch";
                    else if (booklet.StageCode == "EM6002")
                        booklet.Color = "bookletreplaced";
                    else if (booklet.StageCode == "EM4200")
                        booklet.Color = "hostupdate1";
                    else if (booklet.StageCode == "EM4500")
                        booklet.Color = "hostupdate2";

                    if (booklet.DocNo == id)
                    {
                        booklet.Color = booklet.Color + " mainbooklet";
                        result.StageDescription = row["stagecodedesc"].ToString();
                        result.DocumentNo = id;
                    }

                    result.Booklets.Add(booklet);
                }
            }

            // Get other info
            query = @"SELECT ep.formno, aq.queryno, mp.birthdate, ep.doctype, la.description appreasondesc, dp.docno, dp.issuedate, dp.expirydate, 
                            printby, printtime, mp.firstname, mp.surname, cp.othername1, s.description,
                            dp.encodeby, dp.encodetime, qcby, qctime,
                            ep.issueby, ep.issuetime, ep.enrolby, ep.enroltime
                            FROM EnrolProfile ep
                            LEFT JOIN DocHolderMainProfile mp
                            ON ep.idperson = mp.idperson
                            LEFT JOIN DocHolderCustomProfile cp
                            ON ep.idperson = cp.idperson
                            LEFT JOIN afisquery aq
                            ON aq.formno = ep.formno
                            LEFT JOIN DocProfile dp
                            ON ep.formno = dp.formno
                            LEFT JOIN  lookup_appreason la
                            on la.id = ep.appreason
                            LEFT JOIN  stage s
                            on s.stagecode = dp.stagecode
                            WHERE dp.docno = :docno";

            param = new List<OracleParameter>()
            {
                new OracleParameter("docno", id)
            };

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    result.StageDescription = row["description"].ToString();
                    result.Firstname = row["firstname"].ToString();
                    result.Surname = row["surname"].ToString();
                    result.Middlename = row["othername1"].ToString();
                    result.QueryNo = row["appreasondesc"].ToString();
                    result.Dob = TryParseDateTime(row["birthdate"].ToString());
                    result.FormNo = row["formno"].ToString();
                    result.QueryNo = row["queryno"].ToString();
                    result.DocumentType = row["doctype"].ToString();
                    result.DocumentNo = row["docno"].ToString();
                    result.IssueDate = TryParseDateTime(row["issuedate"].ToString());
                    result.ExpiryDate = TryParseDateTime(row["expirydate"].ToString());
                    result.PrintBy = row["printby"].ToString();
                    result.PrintByTime = TryParseDateTime(row["printtime"].ToString());
                    result.EncodedBy = row["encodeby"].ToString();
                    result.EncodedByTime = TryParseDateTime(row["encodetime"].ToString());
                    result.Issuedby = row["issueby"].ToString();
                    result.IssuedbyTime = TryParseDateTime(row["issuetime"].ToString());
                    result.EnrolledBy = row["enrolby"].ToString();
                    result.EnrolledByTime = TryParseDateTime(row["enroltime"].ToString());
                    result.QcBy = row["qcby"].ToString();
                    result.QcByTime = TryParseDateTime(row["qctime"].ToString());
                }
            }


            return result;
        }

        public List<Person> GetApplicationInfo(string id)
        {
            var result = new List<Person>();

            var query = @"SELECT ep.formno , dp.issueplace,  mp.surname, mp.firstname, cp.othername1 , mp.sex, ep.olddocno, mp.birthdate, 
                             mp.birthstate, cp.originstate, cp.addpermanent, cp.mobilephone, ep.idperson,
                             cp.contactphone, cp.email, cp.maritalstatus,
                             cp.nokname, cp.nokaddress,
                             cp.gtoname, cp.gtoaddress,
                             ep.doctype, ep.appreason, la.description appreasondesc, dp.docno, dp.issuedate, dp.expirydate,
                             ep.enrolby, ep.enroltime Enroltime, ep.appby, ep.apptime,
                             dp.encodeby, dp.encodetime, dp.issuetime as dpissuetime,
                             dp.qcby, dp.qctime,
                             ep.issueby, ep.issuetime, ep.remarks, bp.faceimage, bp.signimage
                    FROM EnrolProfile ep
                    LEFT JOIN DocHolderMainProfile mp
                    ON ep.idperson = mp.idperson
                    LEFT JOIN DocHolderCustomProfile cp
                    ON ep.idperson = cp.idperson
                    LEFT JOIN DocProfile dp
                    ON ep.formno = dp.formno
                    LEFT JOIN  lookup_appreason la
                    on la.id = ep.appreason
                    LEFT JOIN DocHolderBioProfile bp
                    on ep.idperson = bp.idperson
                    WHERE ep.idperson = :idperson";

            var param = new List<OracleParameter>()
            {
                new OracleParameter("idperson", id)
            };

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var person = CreateDetailedPerson(row);
                    result.Add(person);

                    // Check if he/she has previous names
                    var app = person.Applications[0];
                    query = "select docno, surname, firstname, othername1, faceimage, signimage, stagecode, birthdate from table(get_docholder_hist_AllTypes_V3(:docno))";
                    var param2 = new List<OracleParameter>()
                    {
                        new OracleParameter("docno", app.DocumentNo)
                    };

                    using (var dset2 = new DataSet())
                    {
                        SetConnectionString("OracleConnectionStringPassportMarth");
                        RunProcedure(query, param2.ToArray(), dset2);

                        foreach (DataRow row2 in dset2.Tables[0].Rows)
                        {
                            if (row2["docno"].ToString() == app.DocumentNo)
                            {
                                if (!string.IsNullOrEmpty(row2["firstname"].ToString()))
                                    person.Firstname = row2["firstname"].ToString();

                                if (!string.IsNullOrEmpty(row2["surname"].ToString()))
                                    person.Surname = row2["surname"].ToString();

                                if (!string.IsNullOrEmpty(row2["othername1"].ToString()))
                                    person.Middlename = row2["othername1"].ToString();

                                if (!string.IsNullOrEmpty(row2["signimage"].ToString()))
                                    person.Signature = (byte[])row2["signimage"];

                                if (!string.IsNullOrEmpty(row2["faceimage"].ToString()))
                                    person.Photo = (byte[])row2["faceimage"];

                                if (!string.IsNullOrEmpty(row2["birthdate"].ToString()))
                                    person.Dob = TryParseDateTime(row2["birthdate"].ToString());

                                if (!string.IsNullOrEmpty(row2["stagecode"].ToString()))
                                    app.StageCode = row2["stagecode"].ToString();
                            }
                        }
                    }
                }
            }

            return result;
        }

        public List<Person> SearchBlacklist(string passportnum)
        {
            var result = new List<Person>();

            var query = @"SELECT b.docno, b.firstname, b.lastname, b.sex, b.birthdate, b.placeofbirth,  
                            r.description reason, b.remark,  b.entrydate, b.lastupdate, b.effectivedate, b.active
                            FROM listdb.personblacklist b
                            INNER JOIN listdb.lookupreason r
                            ON b.reason = r.reasonid
                            WHERE docno = :docno";

            var param = new List<OracleParameter>()
            {
                new OracleParameter("docno", passportnum)
            };

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var person = new Person();
                    person.Applications.Add(new Application());
                    person.Applications[0].DocumentNo = row["docno"].ToString();
                    person.Applications[0].StageDescription = row["reason"].ToString();
                    person.Applications[0].Remarks = row["remark"].ToString();
                    person.Applications[0].EnrolDate = TryParseDateTime(row["entrydate"].ToString());
                    person.Applications[0].ExpiryDate = TryParseDateTime(row["lastupdate"].ToString());
                    person.Applications[0].IssueDate = TryParseDateTime(row["effectivedate"].ToString());
                    person.Firstname = row["firstname"].ToString();
                    person.Surname = row["lastname"].ToString();
                    person.Firstname = row["firstname"].ToString();
                    person.Gender = row["sex"].ToString();
                    person.Dob = TryParseDateTime(row["birthdate"].ToString());
                    person.PlaceOfBirth = row["placeofbirth"].ToString();
                    person.Active = row["active"].ToString();

                    result.Add(person);
                }
            }

            return result;
        }

        public List<Application> GetEnrollmentHistory(string formno)
        {
            var result = new List<Application>();
            var query = @"SELECT aq.entrytime, aq.formno, aq.queryno, dp.docno, aq.appreason, aq.olddocno, aq.stagecode, s.description, aq.remarks, ep.enroltime 
                            FROM afisquery aq
                            left join enrolprofile ep 
                            on ep.formno = aq.formno
                            LEFT JOIN idencraft_dat.stage s
                            on s.stagecode = aq.stagecode
                            LEFT JOIN docprofile dp
                            on dp.formno = aq.formno
                            WHERE aq.formno = '";

            var str = "";
            foreach (var s in formno.Split(','))
            {
                if (str == "")
                    str = s.Trim() + "'";
                else
                    str = str + " or aq.formno = '" + s.Trim() + "'";
            }

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query + str, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    result.Add(SetApplicationData(row));
                }

                // Also search based on enrolprofile in case some results are not in afisquery. Rerun query with enrolprofile as main search
                query = @"SELECT aq.entrytime, ep.formno, aq.queryno, dp.docno, ep.appreason, ep.olddocno, ep.stagecode, s.description, aq.remarks, ep.enroltime 
                        FROM enrolprofile ep
                        left join afisquery aq 
                        on ep.formno = aq.formno
                        LEFT JOIN idencraft_dat.stage s
                        on s.stagecode = ep.stagecode
                        LEFT JOIN docprofile dp
                        on dp.formno = ep.formno
                        WHERE ep.formno = '";

                using (var dset2 = new DataSet())
                {
                    str = string.Empty;

                    foreach (var s in formno.Split(','))
                    {
                        if (str == "")
                            str = s.Trim() + "'";
                        else
                            str = str + " or ep.formno = '" + s.Trim() + "'";
                    }

                    new OracleDataProv().RunProcedure(query + str, null, dset2);
                    foreach (DataRow row in dset2.Tables[0].Rows)
                    {
                        var frm = row["formno"].ToString();

                        if (result.FirstOrDefault(x => x.FormNo == frm) == null)
                            result.Add(SetApplicationData(row));
                    }
                }
            }

            return result.OrderBy(x => x.EnrolDate).ToList();
        }

        public List<Person> GetAfisTransactions(string id, string formno)
        {
            var result = new List<Person>();
            string query;
            List<OracleParameter> param;

            if (!string.IsNullOrEmpty(id) && id != "Pending")
            {
                query = @"SELECT adt.dml_type, aq.formno, adt.stagecode,  adt.adt_date, mp.surname, mp.firstname,
                              adt.remarks  REMARKS, aq.olddocno, adt.flag, adt.flag_, ep.idperson
                              FROM EnrolProfile ep
                              LEFT JOIN DocHolderMainProfile mp
                              ON ep.idperson = mp.idperson
                              LEFT OUTER JOIN AfisQuery aq 
                              ON aq.formno = ep.formno
                              LEFT JOIN AfisQuery_Adt adt
                              ON adt.formno = aq.formno
                              WHERE ep.idperson = :idperson";


                param = new List<OracleParameter>()
                {
                    new OracleParameter("idperson", id)
                };

                using (var dset = new DataSet())
                {
                    new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                    var formid = new List<string>();

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var formnumber = row["formno"].ToString();
                        var trans = new AfistTransaction();
                        if (!string.IsNullOrEmpty(row["adt_date"].ToString()))
                            trans.AdtDate = DateTime.Parse(row["adt_date"].ToString());
                        trans.FormNo = formnumber;
                        trans.StageCode = row["stagecode"].ToString();

                        var dmltype = row["dml_type"].ToString();
                        trans.DmlType = dmltype == "I" ? "Insert" : dmltype == "D" ? "Delete" : dmltype == "U" ? "Update" : "";
                        trans.Remarks = row["REMARKS"].ToString();
                        trans.OldDocNumber = row["olddocno"].ToString();
                        trans.Flag1 = row["flag"].ToString();
                        trans.Flag2 = row["flag_"].ToString();

                        if (formid.Contains(formnumber))
                            result.First(x => x.FormNo == formnumber).AfisRecords.Add(trans);
                        else
                            result.Add(new Person
                            {
                                Firstname = row["firstname"].ToString(),
                                Surname = row["surname"].ToString(),
                                FormNo = formnumber,
                                AfisRecords = new List<AfistTransaction>
                                {
                                    trans
                                }
                            });

                        formid.Add(formnumber);
                    }
                }
            }
            else
            {
                query = @"SELECT adt.dml_type, aq.formno, adt.stagecode,  adt.adt_date,
                              adt.remarks  REMARKS, aq.olddocno, adt.flag, adt.flag_
                              FROM afisquery aq
                              LEFT JOIN AfisQuery_Adt adt
                              ON adt.formno = aq.formno
                              WHERE aq.formno = :formno order by adt.adt_date";


                param = new List<OracleParameter>()
                {
                    new OracleParameter("formno", formno)
                };

                using (var dset = new DataSet())
                {
                    new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                    var person = new Person
                    {
                        Firstname = "Pending", Surname = "Pending", FormNo = formno
                    };

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var trans = new AfistTransaction();
                        if (!string.IsNullOrEmpty(row["adt_date"].ToString()))
                            trans.AdtDate = DateTime.Parse(row["adt_date"].ToString());
                        trans.FormNo = formno;
                        trans.StageCode = row["stagecode"].ToString();

                        var dmltype = row["dml_type"].ToString();
                        trans.DmlType = dmltype == "I" ? "Insert" : dmltype == "D" ? "Delete" : dmltype == "U" ? "Update" : "";
                        trans.Remarks = row["REMARKS"].ToString();
                        trans.OldDocNumber = row["olddocno"].ToString();
                        trans.Flag1 = row["flag"].ToString();
                        trans.Flag2 = row["flag_"].ToString();

                        person.AfisRecords.Add(trans);
                    }

                    result.Add(person);
                }
            }

            return result;
        }

        public List<Branch> GetBranches()
        {
            var result = new List<Branch>();

            var query = @"select branch_code, branch_name, branch_ip from passport_branches order by branch_name";

            using (var dset = new DataSet())
            {
                SetConnectionString("OracleConnectionStringIdoc");
                RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var branch = new Branch
                    {
                        BranchCode = row["branch_code"].ToString(),
                        BranchName = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(row["branch_name"].ToString().ToLower()),
                        Ip = row["branch_ip"].ToString(),
                    };
                    result.Add(branch);
                }
            }

            return result;
        }

        public List<State> GetStates()
        {
            var result = new List<State>();

            var query = @"select id, name from stateorprovince";

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, null, dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    var branch = new State
                    {
                        Id = int.Parse(row["id"].ToString()),
                        Name = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(row["name"].ToString().ToLower())
                    };
                    result.Add(branch);
                }
            }

            return result;
        }

        public Summary GetTableViewData(string id, string formno)
        {
            var result = new Summary();
            string query;

            if (id == "Pending")
            {
                // Fingerprint submission
                query = @"SELECT aq.formno, aq.queryno, aq.entrytime, aq.stagecode , aq.flag , aq.olddocno, aq.doctype, dt.description, la.appreason, aq.remarks FROM AfisQuery aq
                    LEFT JOIN  lookup_appreason la
                    on la.id = aq.appreason
                    LEFT JOIN  lookup_doctype dt
                    on dt.doctype = aq.doctype
                    WHERE aq.formno = :formno";

                var param = new List<OracleParameter>()
                {
                    new OracleParameter("formno", formno)
                };

                using (var dset = new DataSet())
                {
                    new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var fs = new FingerprintSubmission
                        {
                            FormNo = row["formno"].ToString(),
                            QueryNo = row["queryno"].ToString(),
                            EntryTime = TryParseDateTime(row["entrytime"].ToString()),
                            Flag = row["flag"].ToString(),
                            OldDocNo = row["olddocno"].ToString(),
                            DocType = row["doctype"] + " (" + 
                                Helper.ToFirstLetterUpper(row["description"].ToString().Replace(" PASSPORT", "")) + ")",
                            Remarks = row["remarks"].ToString(),
                            StageCode = row["stagecode"].ToString(),
                            AppReason = row["appreason"].ToString()
                        };

                        result.FingerprintSubmission.Add(fs);
                    }
                }

            }
            else
            {
                // Personal Data
                query = @"SELECT mp.surname, mp.firstname, cp.othername1 , mp.sex,  mp.birthdate, ep.formno, 
                            mp.birthstate, cp.originstate
                            FROM EnrolProfile ep
                            LEFT JOIN DocHolderMainProfile mp
                            ON ep.idperson = mp.idperson
                            LEFT JOIN DocHolderCustomProfile cp
                            ON ep.idperson = cp.idperson
                            WHERE ep.idperson = :idperson";

                var param = new List<OracleParameter>()
                {
                    new OracleParameter("idperson", id)
                };

                using (var dset = new DataSet())
                {
                    new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var pd = new PersonalData
                        {
                            Surname = row["surname"].ToString(),
                            Firstname = row["firstname"].ToString(),
                            Middle = row["othername1"].ToString(),
                            FormNo = row["formno"].ToString(),
                            Gender = row["sex"].ToString() == "M"
                                ? "Male"
                                : row["sex"].ToString() == "F"
                                    ? "Female"
                                    : "",
                            BirthDate = TryParseDateTime(row["birthdate"].ToString()),
                            PlaceOfBirth = row["birthstate"].ToString(),
                            StateOfOrigin = row["originstate"].ToString()
                        };

                        result.PersonalData.Add(pd);
                    }

                }

                // Afis records
                query = @"select formno, entrytime, queryno from afisquery where formno IN 
                        (SELECT formno FROM EnrolProfile WHERE idperson = :idperson)";

                param = new List<OracleParameter>
                {
                    new OracleParameter("idperson", id)
                };

                using (var dset = new DataSet())
                {
                    new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var ad = new AfisData
                        {
                            FormNo = row["formno"].ToString(),
                            QueryNo = row["queryno"].ToString(),
                            LastModified = TryParseDateTime(row["entrytime"].ToString())
                        };

                        result.AfisData.Add(ad);
                    }

                    // Set last edit date
                    if (result.AfisData.Count > 1)
                    {
                        var orderedafis = result.AfisData.OrderBy(x => x.LastModified).ToList();
                        var first = orderedafis.First();

                        foreach (var afisdata in result.AfisData)
                        {
                            if (afisdata.FormNo != first.FormNo)
                            {
                                afisdata.CreationDate = first.LastModified;
                            }
                        }
                    }

                }

                // Fingerprint submission
                query = @"SELECT aq.formno, aq.queryno, aq.entrytime, aq.stagecode , aq.flag , aq.olddocno, aq.doctype, dt.description, 
                    la.appreason, aq.remarks FROM AfisQuery aq
                    LEFT JOIN EnrolProfile ep
                    ON ep.formno = aq.formno
                    LEFT JOIN  lookup_appreason la
                    on la.id = aq.appreason
                    LEFT JOIN  lookup_doctype dt
                    on dt.doctype = aq.doctype
                    WHERE aq.formno IN (SELECT formno FROM EnrolProfile WHERE idperson = :idperson)";

                param = new List<OracleParameter>
                {
                    new OracleParameter("idperson", id)
                };

                using (var dset = new DataSet())
                {
                    new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var fs = new FingerprintSubmission
                        {
                            FormNo = row["formno"].ToString(),
                            QueryNo = row["queryno"].ToString(),
                            EntryTime = TryParseDateTime(row["entrytime"].ToString()),
                            Flag = row["flag"].ToString(),
                            OldDocNo = row["olddocno"].ToString(),
                            DocType = row["doctype"] + " (" + 
                                Helper.ToFirstLetterUpper(row["description"].ToString().Replace(" PASSPORT", "")) + ")",
                            Remarks = row["remarks"].ToString(),
                            StageCode = row["stagecode"].ToString(),
                            AppReason = row["appreason"].ToString()
                        };

                        result.FingerprintSubmission.Add(fs);
                    }
                }

                // Basic Enrolment Data
                query = @"SELECT aq.oldformno, ep.olddocno, ep.doctype, ep.enrolby, ep.enroltime, ep.issueby, ep.formno, ep.stagecode, dt.description,
                            ep.issuetime, ep.remarks
                            FROM EnrolProfile ep
                            LEFT JOIN AfisQuery aq
                            ON ep.formno = aq.formno
                            LEFT JOIN  lookup_doctype dt
                            on dt.doctype = ep.doctype
                            WHERE ep.idperson = :idperson";

                param = new List<OracleParameter>()
                {
                    new OracleParameter("idperson", id)
                };

                using (var dset = new DataSet())
                {
                    new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var ed = new EnrolmentData
                        {
                            OldFormNo = row["oldformno"].ToString(),
                            EnrolTime = TryParseDateTime(row["Enroltime"].ToString()),
                            OldDocNo = row["olddocno"].ToString(),
                            StageCode = row["stagecode"].ToString(),
                            DocType = row["doctype"] + " (" + 
                                Helper.ToFirstLetterUpper(row["description"].ToString().Replace(" PASSPORT", "")) + ")",
                            EnrolBy = row["enrolby"].ToString(),
                            FormNo = row["formno"].ToString()
                        };
                        ed.EnrolTime = TryParseDateTime(row["issuetime"].ToString());
                        ed.Remarks = row["remarks"].ToString();

                        result.EnrolmentData.Add(ed);
                    }
                }

                // Document Information
                query = @"SELECT ep.formno, ep.doctype, dp.docno, dp.issuedate, dp.expirydate,
                            ep.issueby, ep.issuetime, dp.stagecode, dt.description
                            FROM EnrolProfile ep
                            LEFT JOIN DocProfile dp
                            ON ep.formno = dp.formno
                            LEFT JOIN  lookup_doctype dt
                            on dt.doctype = ep.doctype
                            WHERE ep.idperson = :idperson";

                param = new List<OracleParameter>()
                {
                    new OracleParameter("idperson", id)
                };

                using (var dset = new DataSet())
                {
                    new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var di = new DocumentInfo
                        {
                            FormNo = row["formno"].ToString(),
                            IssueTime = TryParseDateTime(row["issuetime"].ToString()),
                            DocType = row["doctype"] + " (" +
                                      Helper.ToFirstLetterUpper(row["description"].ToString().Replace(" PASSPORT", "")) + ")",
                            DocNo = row["docno"].ToString(),
                            IssueBy = row["issueby"].ToString(),
                            StageCode = row["stagecode"].ToString()
                        };

                        result.DocumentInfo.Add(di);
                    }
                }

                // Personalisation Information
                query = @"SELECT ep.formno, pp.docno, pp.persotime, pp.stagecode, pp.remarks
                            FROM EnrolProfile ep
                            LEFT JOIN PersoProfile pp
                            ON ep.formno = pp.formno
                            WHERE ep.idperson = :idperson";

                param = new List<OracleParameter>
                {
                    new OracleParameter("idperson", id)
                };

                using (var dset = new DataSet())
                {
                    new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                    foreach (DataRow row in dset.Tables[0].Rows)
                    {
                        var pi = new PersonalisationInfo
                        {
                            FormNo = row["formno"].ToString(),
                            PersoTime = TryParseDateTime(row["persotime"].ToString()),
                            DocNo = row["docno"].ToString(),
                            Remarks = row["remarks"].ToString(),
                            StageCode = row["stagecode"].ToString()
                        };

                        result.PersonalisationInfo.Add(pi);
                    }
                }
            }

            return result;
        }

        public List<PassportReport> GetPassportsIssued(DateTime start, DateTime end)
        {
            var passportReportList = new List<PassportReport>();
            using (var dataSet = new DataSet())
            {
                var query = @"select b.branchcode, b.branchname, d.doctype, count(*) as count from docprofile d
                                INNER join branch b on b.branchcode = d.branchcode where issuedate is not null 
                                and issuedate between to_date('" + start.ToString("dd-MMM-yyyy") + @" 00:00:00', 'DD-Mon-YYYY HH24:MI:SS')
                                and to_date('" + end.ToString("dd-MMM-yyyy") + " 23:59:59', 'DD-Mon-YYYY HH24:MI:SS') group by b.branchcode, " +
                                "b.branchname, d.doctype order by branchname";
                new OracleDataProv().RunProcedure(query, null, dataSet);
                foreach (DataRow row in dataSet.Tables[0].Rows)
                    passportReportList.Add(new PassportReport
                    {
                        DocType = row["doctype"].ToString(),
                        BranchCode = int.Parse(row["branchcode"].ToString()),
                        NumberIssued = int.Parse(row["count"].ToString())
                    });
            }

            var dictionary = new Dictionary<int, string>();
            using (var dataSet = new DataSet())
            {
                SetConnectionString("OracleConnectionStringIdoc");
                RunProcedure("select branch_code, branch_name_edit, is_foreign from passport_branches", null, dataSet);
                foreach (DataRow row in dataSet.Tables[0].Rows)
                    dictionary.Add(int.Parse(row["branch_code"].ToString()), row["branch_name_edit"] + "," + row["is_foreign"]);
            }

            var intList = new List<int>();
            foreach (var passportReport in passportReportList)
            {
                if (dictionary.ContainsKey(passportReport.BranchCode))
                {
                    passportReport.BranchName = dictionary[passportReport.BranchCode].Split(',').First();
                    passportReport.IsForeign = int.Parse(dictionary[passportReport.BranchCode].Split(',').Last());

                    if (!intList.Contains(passportReport.BranchCode))
                        intList.Add(passportReport.BranchCode);
                }
            }

            var source = new List<PassportReport>();
            foreach (var keyValuePair in dictionary)
            {
                if (!intList.Contains(keyValuePair.Key))
                    source.Add(new PassportReport
                    {
                        BranchCode = keyValuePair.Key,
                        BranchName = keyValuePair.Value.Split(',').First(),
                        DocType = "P",
                        IsForeign = int.Parse(keyValuePair.Value.Split(',').Last()),
                        NumberIssued = 0
                    });
            }

            passportReportList.AddRange(source.OrderBy((x => x.BranchName)).ToList());
            return passportReportList;
        }

        public LocPassport GetPassportDetails(string formno, bool bio)
        {
            var result = new LocPassport();
            var data = new OracleDataProv();

            // Get Enrolprofile
            using (var dataSet = new DataSet())
            {
                var query = @"select * from enrolprofile where formno = :formno";
                var param = new List<OracleParameter>
                {
                    new OracleParameter("formno", formno)
                };
                data.RunProcedure(query, param.ToArray(), dataSet);
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    result.LocEnrolProfile = dataSet.Tables[0].ToList<LocEnrolProfile>().FirstOrDefault();
                }
            }

            // Get Mainprofile
            using (var dataSet = new DataSet())
            {
                var query = @"select * from DocHolderMainProfile where idperson = :idperson";
                var param = new List<OracleParameter>
                {
                    new OracleParameter("idperson", result.LocEnrolProfile?.Idperson)
                };
                data.RunProcedure(query, param.ToArray(), dataSet);
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    result.LocDocHolderMainProfile = dataSet.Tables[0].ToList<LocDocHolderMainProfile>().FirstOrDefault();
                }
            }

            // Get Customprofile
            using (var dataSet = new DataSet())
            {
                var query = @"select * from DocHolderCustomProfile where idperson = :idperson";
                var param = new List<OracleParameter>
                {
                    new OracleParameter("idperson", result.LocEnrolProfile?.Idperson)
                };
                data.RunProcedure(query, param.ToArray(), dataSet);
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    result.LocDocHolderCustomProfile = dataSet.Tables[0].ToList<LocDocHolderCustomProfile>().FirstOrDefault();
                }
            }

            // Get Paymentprofile
            using (var dataSet = new DataSet())
            {
                var query = @"select * from PaymentHistory where formno = :formno";
                var param = new List<OracleParameter>
                {
                    new OracleParameter("formno", result.LocEnrolProfile?.Formno)
                };
                data.RunProcedure(query, param.ToArray(), dataSet);
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    result.LocPaymentHistory = dataSet.Tables[0].ToList<LocPaymentHistory>().FirstOrDefault();
                }
            }

            if (bio)
            {
                // Get Bioprofile
                using (var dataSet = new DataSet())
                {
                    var query = @"select * from DocHolderBioProfile where idperson = :idperson";
                    var param = new List<OracleParameter>
                    {
                        new OracleParameter("idperson", result.LocEnrolProfile?.Idperson)
                    };
                    data.RunProcedure(query, param.ToArray(), dataSet);
                    if (dataSet.Tables[0].Rows.Count != 0)
                    {
                        result.LocDocHolderBioProfile = dataSet.Tables[0].ToList<LocDocHolderBioProfile>()
                            .FirstOrDefault();
                    }
                }
            }

            return result;
        }

        public LocPassport GetPassportDetailsDocNo(string docno, bool bio)
        {
            var result = new LocPassport();
            var data = new OracleDataProv();

            // Get docprofile
            using (var dataSet = new DataSet())
            {
                var query = @"select * from docprofile where docno = :docno";
                var param = new List<OracleParameter>
                {
                    new OracleParameter("docno", docno)
                };
                data.RunProcedure(query, param.ToArray(), dataSet);
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    result.LocDocProfile = dataSet.Tables[0].ToList<LocDocProfile>().FirstOrDefault();
                }
            }

            // Get enrolprofile
            using (var dataSet = new DataSet())
            {
                var query = @"select * from enrolprofile where formno = :formno";
                var param = new List<OracleParameter>
                {
                    new OracleParameter("formno", result.LocDocProfile?.Formno)
                };
                data.RunProcedure(query, param.ToArray(), dataSet);
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    result.LocEnrolProfile = dataSet.Tables[0].ToList<LocEnrolProfile>().FirstOrDefault();
                }
            }

            // Get Mainprofile
            using (var dataSet = new DataSet())
            {
                var query = @"select * from DocHolderMainProfile where idperson = :idperson";
                var param = new List<OracleParameter>
                {
                    new OracleParameter("idperson", result.LocEnrolProfile?.Idperson)
                };
                data.RunProcedure(query, param.ToArray(), dataSet);
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    result.LocDocHolderMainProfile = dataSet.Tables[0].ToList<LocDocHolderMainProfile>().FirstOrDefault();
                }
            }

            // Get Customprofile
            using (var dataSet = new DataSet())
            {
                var query = @"select * from DocHolderCustomProfile where idperson = :idperson";
                var param = new List<OracleParameter>
                {
                    new OracleParameter("idperson", result.LocEnrolProfile?.Idperson)
                };
                data.RunProcedure(query, param.ToArray(), dataSet);
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    result.LocDocHolderCustomProfile = dataSet.Tables[0].ToList<LocDocHolderCustomProfile>().FirstOrDefault();
                }
            }

            // Get Paymentprofile
            using (var dataSet = new DataSet())
            {
                var query = @"select * from PaymentHistory where formno = :formno";
                var param = new List<OracleParameter>
                {
                    new OracleParameter("formno", result.LocEnrolProfile?.Formno)
                };
                data.RunProcedure(query, param.ToArray(), dataSet);
                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    result.LocPaymentHistory = dataSet.Tables[0].ToList<LocPaymentHistory>().FirstOrDefault();
                }
            }

            if (bio)
            {
                // Get Bioprofile
                using (var dataSet = new DataSet())
                {
                    var query = @"select * from DocHolderBioProfile where idperson = :idperson";
                    var param = new List<OracleParameter>
                    {
                        new OracleParameter("idperson", result.LocEnrolProfile?.Idperson)
                    };
                    data.RunProcedure(query, param.ToArray(), dataSet);
                    if (dataSet.Tables[0].Rows.Count != 0)
                    {
                        result.LocDocHolderBioProfile = dataSet.Tables[0].ToList<LocDocHolderBioProfile>()
                            .FirstOrDefault();
                    }
                }
            }

            return result;
        }

        #region Private Operations

        private static IEnumerable<string> GetFormnosFromSearchingByOldDocNo(string docno)
        {
            var result = new List<string>();

            var query = @"select aq.formno, aq.oldformno 
                            from afisquery aq
                            left join docprofile d 
                            on d.formno = aq.formno
                            where d.docno = :docno";

            var param = new List<OracleParameter>
            {
                new OracleParameter("docno", docno)
            };

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    result.Add(row["formno"].ToString());

                    // if there is another old docno attached to this, retreive that too
                    if (!string.IsNullOrEmpty(row["oldformno"].ToString()))
                    {
                        // Get the docNo of the form
                        var newdocno = GetDocNoFromFormNo(row["oldformno"].ToString());

                        // Repeat process
                        if (!string.IsNullOrEmpty(newdocno))
                            result.AddRange(GetFormnosFromSearchingByOldDocNo(newdocno));
                    }
                }
            }

            return result;
        }

        private IEnumerable<string> GetFormnosFromSearchingForDocNo(string docno)
        {
            var result = new List<string>();

            var query = @"select aq.formno, aq.oldformno 
                            from afisquery aq
                            left join docprofile d 
                            on d.formno = aq.oldformno
                            where d.docno = :docno";

            var param = new List<OracleParameter>
            {
                new OracleParameter("docno", docno)
            };

            using (var dset = new DataSet())
            {
                RunProcedure(query, param.ToArray(), dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    result.Add(row["formno"].ToString());

                    // if there is another old docno attached to this, retreive that too
                    if (!string.IsNullOrEmpty(row["formno"].ToString()))
                    {
                        // Get the docNo of the form
                        var newdocno = GetDocNoFromFormNo(row["formno"].ToString());

                        // Repeat process
                        if (!string.IsNullOrEmpty(newdocno))
                            result.AddRange(GetFormnosFromSearchingForDocNo(newdocno));
                    }
                }
            }

            return result;
        }

        private static IEnumerable<string> GetFormnosFromSearchingByOldFormNo(string formno)
        {
            var result = new List<string>();

            var query = @"select aq.formno, aq.oldformno 
                            from afisquery aq
                            where aq.formno = :formno";

            var param = new List<OracleParameter>
            {
                new OracleParameter("formno", formno)
            };

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    result.Add(row["formno"].ToString());

                    // if there is another old docno attached to this, retreive that too
                    if (!string.IsNullOrEmpty(row["oldformno"].ToString()))
                    {
                        // Repeat process
                        result.AddRange(GetFormnosFromSearchingByOldFormNo(row["oldformno"].ToString()));
                    }
                }
            }

            return result;
        }

        private static IEnumerable<string> GetFormnosFromSearchingForFormNo(string formno)
        {
            var result = new List<string>();

            var query = @"select formno, oldformno 
                            from afisquery
                            where oldformno = :formno";

            var param = new List<OracleParameter>
            {
                new OracleParameter("formno", formno)
            };

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    result.Add(row["formno"].ToString());

                    // Repeat to get all
                    result.AddRange(GetFormnosFromSearchingForFormNo(row["formno"].ToString()));
                }
            }

            return result;
        }

        private static string GetDocNoFromFormNo(string formno)
        {
            var query = "select docno from docprofile where formno = :formno";
            var param = new List<OracleParameter>
            {
                new OracleParameter("formno", formno)
            };

            using (var dset = new DataSet())
            {
                new OracleDataProv().RunProcedure(query, param.ToArray(), dset);

                if (dset.Tables[0].Rows.Count != 0)
                    return dset.Tables[0].Rows[0][0].ToString();

                return string.Empty;
            }
        }

        private static Application SetApplicationData(DataRow row)
        {
            var app = new Application();

            if (!string.IsNullOrEmpty(row["enroltime"].ToString()))
                app.EnrolDate = DateTime.Parse(row["enroltime"].ToString());
            else if (!string.IsNullOrEmpty(row["entrytime"].ToString()))
                app.EnrolDate = DateTime.Parse(row["entrytime"].ToString());
            app.FormNo = row["formno"].ToString();
            app.QueryNo = row["queryno"].ToString();
            app.DocumentNo = row["docno"].ToString();
            app.Appr = row["appreason"].ToString();
            app.OldDocumentNo = row["olddocno"].ToString();
            app.StageCode = row["stagecode"].ToString();
            app.StageDescription = row["description"].ToString();

            // temporary
            if (app.StageCode == "EM1501")
                app.StageDescription = "AFIS Failed";

            app.Remarks = row["remarks"].ToString();

            return app;
        }

        private static Person CreatePerson(DataRow row)
        {
            var person = new Person
            {
                Surname = row["surname"].ToString(),
                Firstname = row["firstname"].ToString(),
                Middlename = row["othername1"].ToString(),
                Gender = row["sex"].ToString() == "M" ? "Male" : "Female",
                AppType = row["doctype"].ToString().ToUpper()
            };

            if (!string.IsNullOrEmpty(row["birthdate"].ToString()))
                person.Dob = DateTime.Parse(row["birthdate"].ToString());
            person.IdPerson = row["idperson"].ToString();

            person.FormNoList.Add(string.IsNullOrEmpty(row["formno"].ToString()) ? row["epformno"].ToString()
                : row["formno"].ToString());

            return person;
        }

        private static Person CreateDetailedPerson(DataRow row)
        {
            var person = new Person
            {
                IdPerson = row["idperson"].ToString(),
                Surname = row["surname"].ToString(),
                Firstname = row["firstname"].ToString(),
                Middlename = row["othername1"].ToString(),
                Gender = row["sex"].ToString(),
                Photo = (byte[]) row["faceimage"]
            };
            if (!string.IsNullOrEmpty(row["signimage"].ToString()))
                person.Signature = (byte[])row["signimage"];
            person.Dob = TryParseDateTime(row["birthdate"].ToString());
            person.PlaceOfBirth = row["birthstate"].ToString();
            person.StateOfOrigin = row["originstate"].ToString();
            person.HomeAddress = row["addpermanent"].ToString();
            if (person.Gender.ToUpper() == "M")
                person.Title = "Mr.";
            else
            {
                person.Title = row["maritalstatus"].ToString() == "2" ? "Mrs." : "Miss.";
            }

            var dptime = TryParseDateTime(row["dpissuetime"].ToString());
            person.Applications.Add(new Application()
            {
                MobileNumber = row["mobilephone"].ToString(),
                ContactNumber = row["contactphone"].ToString(),
                EmailAddress = row["email"].ToString(),
                Nok = new Person()
                {
                    NameString = row["nokname"].ToString(),
                    HomeAddress = row["nokaddress"].ToString()
                },
                GuarantorAdress = row["gtoaddress"].ToString(),
                GuarantorName = row["gtoname"].ToString(),
                ApplicationType = row["doctype"].ToString(),
                FormNo = row["formno"].ToString(),
                DocumentNo = row["docno"].ToString(),
                OldDocumentNo = row["olddocno"].ToString(),
                IssueDate = TryParseDateTime(row["issuedate"].ToString()),
                IssuingBranch = row["issueplace"].ToString(),
                ExpiryDate = TryParseDateTime(row["expirydate"].ToString()),
                EnrolledBy = Helper.ToFirstLetterUpper(row["enrolby"].ToString()),
                EnrolledByTime = TryParseDateTime(row["enroltime"].ToString()),
                ApprovedBy = Helper.ToFirstLetterUpper(row["appby"].ToString()),
                ApprovedByTime = TryParseDateTime(row["apptime"].ToString()),
                EncodedBy = Helper.ToFirstLetterUpper(row["encodeby"].ToString()),
                EncodedByTime = TryParseDateTime(row["encodetime"].ToString()),
                QcBy = Helper.ToFirstLetterUpper(row["qcby"].ToString()),
                QcByTime = TryParseDateTime(row["qctime"].ToString()),
                Issuedby = Helper.ToFirstLetterUpper(row["issueby"].ToString()),
                IssuedbyTime = dptime == DateTime.MinValue ? TryParseDateTime(row["issuetime"].ToString()) : dptime,
                Remarks = row["remarks"].ToString(),
                Appr = row["appreason"].ToString(),
                ApprString = row["appreasondesc"].ToString()
            });

            

            

            return person;
        }

        private static DateTime TryParseDateTime(string text)
        {
            DateTime date;
            if (DateTime.TryParse(text, out date))
            {
                return date;
            }
            else
            {
                return DateTime.MinValue;
            }
        }

        private static string GetFirstLetters(string str)
        {
            var result = "";

            foreach (var c in str)
            {
                if (char.IsNumber(c))
                    break;

                result += c;
            }

            return result;
        }

        #endregion


    }

    public static class DataTableExtensions
    {
        public static IList<T> ToList<T>(this DataTable table) where T : new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            IList<T> result = new List<T>();

            foreach (var row in table.Rows)
            {
                var item = CreateItemFromRow<T>((DataRow)row, properties);
                result.Add(item);
            }

            return result;
        }

        private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
        {
            var item = new T();
            foreach (var property in properties)
            {
                var value = row[property.Name];

                if (value is DBNull)
                {
                    property.SetValue(item, property.PropertyType == typeof(string) ? "" : null, null);
                }
                else
                {
                    var t = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                    var safeValue = (value == null) ? null : Convert.ChangeType(value, t);

                    property.SetValue(item, safeValue, null);
                }
            }
            return item;
        }
    }
}
