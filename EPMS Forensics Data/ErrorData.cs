﻿using EPMS_Forensics_Entities;
using System;
using System.Data.OracleClient;

namespace EPMS_Forensics_Data
{
    public class ErrorData: OracleDataProv
    {
        public void SaveError(Error error)
        {
            SetConnectionString("OracleConnectionStringIdoc");
            var query = "insert into errorlogs (message, stacktrace, innerex_msg, innerex_source, innerex_trace, time_logged, is_app_error) values (:message, :stacktrace, :innerex_msg, :innerex_source, :innerex_trace, :time_logged, :is_app_error)";
            OracleParameter[] param =
                    {   
                        new OracleParameter("message",  error.Message),
                        new OracleParameter("stacktrace",  error.StackTrace),
                        new OracleParameter("innerex_msg",  error.InnerExMsg),
                        new OracleParameter("innerex_source",  error.InnerExSrc),
                        new OracleParameter("innerex_trace",  error.InnerExStkTrace),
                        new OracleParameter("time_logged",  DateTime.Now.ToUniversalTime()),
                        new OracleParameter("is_app_error",  error.AppError),
                    };

            ExecuteQuery(query, param);
        }
    }
}
