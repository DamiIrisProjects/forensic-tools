﻿using System;
using System.Security.Cryptography;

namespace EPMS_Forensics_Data
{
    public sealed class SaltedHash
    {
        public string Salt => _salt;
        public string Hash => _hash;

        public static SaltedHash Create(string password)
        {
            var salt = _createSalt();
            var hash = _calculateHash(salt, password);
            return new SaltedHash(salt, hash);
        }

        public static SaltedHash Create(string salt, string hash)
        {
            return new SaltedHash(salt, hash);
        }

        public bool Verify(string password)
        {
            var h = _calculateHash(_salt, password);
            return _hash.Equals(h);
        }

        private SaltedHash(string s, string h)
        {
            _salt = s;
            _hash = h;
        }

        private static string _createSalt()
        {
            var r = _createRandomBytes(SaltLength);
            return Convert.ToBase64String(r);
        }

        private static byte[] _createRandomBytes(int len)
        {
            var r = new byte[len];
            new RNGCryptoServiceProvider().GetBytes(r);
            return r;
        }

        private static string _calculateHash(string salt, string password)
        {
            var data = _toByteArray(salt + password);
            var hash = _calculateHash(data);
            return Convert.ToBase64String(hash);
        }

        private static byte[] _calculateHash(byte[] data)
        {
            return new SHA1CryptoServiceProvider().ComputeHash(data);
        }

        private static byte[] _toByteArray(string s)
        {
            return System.Text.Encoding.UTF8.GetBytes(s);
        }

        private readonly string _salt;
        private readonly string _hash;
        private const int SaltLength = 6;
    }
}
